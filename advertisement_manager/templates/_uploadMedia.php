<?php
    include_partial(
        'uploadMedia.js',
        array(
            'idChannel' => $idChannel,
            'idAdvertisement' => $idAdvertisement
        )
    );
     
    $maxFileLimit   = 1;
    $maxImageLimit  = sfConfig::get('app_max_image_upload_limit');
    $maxVideoLimit  = sfConfig::get('app_max_video_upload_limit');
    $landing_image_width = sfConfig::get('app_thumb_landing_image_width');
    $landing_image_height = sfConfig::get('app_thumb_landing_image_height');
    $messages       = __('err_invalid_media_file').'|'.sprintf(__('msg_maximum_upload_media_size'),
                    $maxImageLimit, $maxVideoLimit);
    $fileType       = strtolower(sfConfig::get('app_media_main_type_media'));
    $uploadSizeText = sprintf(__('txt_upload_media_ad'), $landing_image_width,$landing_image_height,$maxImageLimit);
    $uploadSizeTextAllow = __('txt_upload_media_allow');
    $url        = '@upload_advertisement_media?update=media&method=POST';
    $url        .= ($idAdvertisement) ? '&id_advertisement='.$idAdvertisement : '';
    $url        .= ($idChannel) ? '&id_channel='.$idChannel : '';
    $url        .= ($idMedia) ? '&id_media='.$idMedia : '';
    $url        = url_for($url);

    echo javascript_tag("
        jQuery(document).ready(function() {
            createUploader('".$fileType."', 1, '/', '".$url."', '".$messages."', 'newFileMedia');
        });
    ");
    
    echo javascript_tag("
        jQuery(document).ready(function() {
            jQuery( '#uploadprogressbar' ).progressbar();
            jQuery('#editStoryTabDiv').attr('style', 'padding-top: 0px; padding-bottom: 0px; overflow: auto;')
        });
    ");
    //jQuery( '#editStoryTabDiv' ).hide();
    $idChannel  = $sf_params->get('id_channel');
    $idArticle  = $sf_params->get('id_article');

    echo input_hidden_tag('id_channel', $idChannel, array('readonly' => true));
    echo input_hidden_tag('id_advertisement', $idAdvertisement, array('readonly' => true));
    $errorHandler = $sf_data->getRaw('errorHandler');
?>
<div>
    <div>
        <div id="uploadMediaTabDiv">
            <?php if($errorHandler != '' && $errorHandler->hasError('advertise_media') ):?>
                <span class="redText" id="fileError"><?php echo $errorHandler->getError('advertise_media');?></span>
            <?php else: ?>
                <?php if($sf_user->hasFlash('success_media')): ?>
                    <div id="successClassUpload" class="mediaSuccessClass" style="width:100%;margin-left:0px;">
                        <div class="success canhide" style="margin-left:0px;"><?php echo $sf_user->getFlash('success_media'); ?></div>
                    </div>
                <?php else: ?>
                    <div id="successClassUpload" class="mediaSuccessClass PT10" style="width:100%; display:none;">
                        <div class="success canhide" style="margin-left:0px;"><?php echo __('msg_file_uploaded'); ?></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="error_list1">
                <ul class="error_list"><li></li></ul>
            </div>
            
            <div class="boxW2">
                <div class="boxW2TiSub uploadImageText"><?php echo __('lbl_upload_media'); ?></div>
            
                <div class="boxW2Sub paddTB35LR10">
                   <div class="boxW2DropText">
                       <?php echo sprintf(__('lbl_drag_and_drop'), 'image or video'); ?>
                   </div>
                   <div class="boxW2Select width110" align="center">
                        <span id="newFileMedia"></span>
                   </div>
                </div>

                <div class="boxW2Midd">
                    <div class="boxW2MidTop">
                        <div class="boxW2MidTopNormal validForFileSize"><?php echo $uploadSizeText; ?><br/><?php echo $uploadSizeTextAllow; ?></div>
                        
                        <!--    progress bar    -->
                        <div id="uploadprogressbar" style="display:none;"></div>
                        <!--    end of progress bar    -->
            
                        <div class="mediaList" id="mediaList">
                        <?php
                            /*if(count($medias) > 0):
                                include_partial(
                                    'seeMoreUrl',
                                    array(
                                        'idChannel'    => $idChannel,
                                        'idAdvertisement'    => $idAdvertisement,
                                        'media_type'   => $mediaType,
                                        'medias'       => $medias
                                    )
                                );
                            endif;*/
                        ?>
                        </div>
                    </div>
                </div>
            
                <div class="seemoreLink" id="seemore" style="display:none;"></div>
                <div style="color:red; display:none;" id="errorMsg">&nbsp;</div>
                <div id="addEditMediaMetadata"></div>
            </div>
        </div>
        <!--    end of upload media    -->
    </div>

    <div class="divclear"></div>
    
    <div id="successClassPublish" style="padding:10px 10px 0px 10px; display:none;">
        <div class="success canhide MT0" id="successClassPublishInner"></div>
    </div>
</div>
