<?php
    use_helper('JavascriptBase', 'jQuery', 'DateForm');
    echo stylesheet_tag('story.css');
    //  datepicker
    use_stylesheet('dialog/jquery-ui-1.8.20.custom.css');
    use_javascript('dialog/jquery-1.7.2.min.js');
    use_javascript('dialog/jquery-ui-1.8.20.custom.min.js');
    //  datepicker
    
    if($sf_user->hasFlash('success_advertisement') && $sf_user->getFlash('success_advertisement') != "" && $ssMode != 'Draft'){
       echo javascript_tag('window.location.href="/advertisement"');
    }
    if($sf_user->hasFlash('success_advertisement') && $sf_user->getFlash('success_advertisement') != "" && $ssMode == 'Draft'){
       echo javascript_tag('window.location.href="/advertisement"');
    }
    echo javascript_tag("
        jQuery(function() {
            jQuery('.articlecontent').css('display','none'); 
        });
    ");    

    $imagePath      = sfConfig::get('sf_web_dir').sfConfig::get('app_view_path_article_landing_image');
    $articleImg = "";
    $articleNoImg = sfConfig::get('app_story_bg_image');
 ?>
<div style="position:absolute; width:100%; background:#000000; display:none; overflow:hidden;" align="center" id="advertiseBgImage">
    <?php 
        if(count($advertisementForm->getErrorSchema()) == 0):
            echo image_tag((($articleImg != "")?$articleImg:$articleNoImg), array('id'=>'createAdvertiseBG'));
        endif;
    ?>
</div>
<div class="loaderimgText" style="float:right; display:none; margin:0px; left:0px;" id="createStoryInd">
    <?php echo image_tag('loader.gif'); ?><div class="backTransparant"></div>
</div>

<?php
    if(count($advertisementForm->getErrorSchema()) == 0):
        $snImgHeight = sfConfig::get('app_story_img_height');
        $snImgWidth = sfConfig::get('app_story_img_width');
        if($articleImg == ""){
            $imagepath = 'http://'.$sf_request->getHost().'/images/'.$articleNoImg;
        }else{
            $imagepath = 'http://'.$sf_request->getHost().$articleImg;
        }
        echo javascript_tag("
            jQuery(document).ready(function() {
                var newsize = resizeBackground(jQuery(window).width(),(jQuery(window).height()), ".$snImgWidth.",".$snImgHeight.");
                jQuery('#createAdvertiseBG').attr('width',Math.round(newsize[0])).attr('height',Math.round(newsize[1])); 
                xImage = new Image;
                xImage.src = '$imagepath';
                xImage.onload = function() {
                  jQuery('#advertiseBgImage').fadeIn('fast'); 
                }
            });
        ");
    endif;

    echo javascript_tag("
            jQuery(document).ready(function() {                // Prevent selection of invalid dates through the select controls
                jQuery('#datepicker_start').datepicker({
                    showOn: 'button',
                    buttonImage: '/images/calender.png',
                    buttonImageOnly: true,
                    dateFormat: '".sfConfig::get('app_date_format')."',
                    onSelect: function(dateText, inst) {
                        //dateText comes in as mm/dd/y
                        var datePieces = dateText.split('/');
                        var month = datePieces[0];
                        var day = datePieces[1];
                        var year = datePieces[2];
                        //corresponding element
                        jQuery('#publish_start_month').val(month);
                        jQuery('#publish_start_day').val(day);
                        jQuery('#publish_start_year').val(year);
                        jQuery('#start_date').val(dateText);
                    }
                });
                    
                jQuery('#datepicker_end').datepicker({
                    showOn: 'button',
                    buttonImage: '/images/calender.png',
                    buttonImageOnly: true,
                    dateFormat: '".sfConfig::get('app_date_format')."',
                    onSelect: function(dateText, inst) {
                        //dateText comes in as mm/dd/y
                        var datePieces = dateText.split('/');
                        var month = datePieces[0];
                        var day = datePieces[1];
                        var year = datePieces[2];
                        //corresponding element
                        jQuery('#publish_end_month').val(month);
                        jQuery('#publish_end_day').val(day);
                        jQuery('#publish_end_year').val(year);
                        jQuery('#end_date').val(dateText);
                    }
                });
                
                setFocus('headline');
                

            });
        ");
    
    
 ?>
<div class="addEditStoryModeBoxMain" id="addEditAdvertisement">
	<div class="addEditStoryModeBox">
	    
	<?php
        echo $advertisementForm->renderFormTag(
            url_for('@add_edit_advertise'),
            array( 
                "name"      => $advertisementForm->getName(), 
                "id"        => 'frm'.$advertisementForm->getName(),
                "method"    => "post"
            )
        );
        
        echo $advertisementForm->renderHiddenFields();
        //if($advertisementForm->isNew()){
            echo input_hidden_tag('advertisement[id]',$idAdvertisement);
        //}
        echo input_hidden_tag('id_advertisement',$idAdvertisement,array('id'=>'extra_advertisement_id'));
        echo input_hidden_tag('id_channel',$idChannel);
        echo input_hidden_tag('fileName',$fileName);
    ?>
    <div class="addEditStoryTitleBox">
     	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
           <td align="left" valign="top">
                            <span class="first_td headlineLabel">
                                <?php echo $advertisementForm['headline']->renderLabel();?>:<span class="redText">
                                <?php
                                    if($advertisementForm['headline']->hasError()){
                                        echo $advertisementForm['headline']->getError();
                                    }
                                 ?></span>
                            </span>
                                    <div class="uploadImageText" style="float:right; " id="articleStatus">
                                        <?php
                                            if(isset($ssMode) && $ssMode != "" && $ssMode == 'Draft' && count($advertisementForm->getErrorSchema()) == 0 && $reQ != 'initial'){
                                                echo __('msg_status_draft');  
                                            }else if( ($idAdvertisement != "" || ( isset($ssMode) && $ssMode == '')) && count($advertisementForm->getErrorSchema()) == 0 && $reQ != 'initial'){
                                                echo __('msg_status_live');
                                            }else{
                                                 echo __('msg_status_new');
                                            }
                                         ?>
                                    </div><br />
                            <?php 
                                echo $advertisementForm['headline']->render(array('class' =>'articleFormboxTextfilde headlineText'));
                                echo javascript_tag("setFocus('advertisement_headline');");
                            ?>
                            <?php if($idAdvertisement): ?>
                                <span class="headlineUpdated">
                                    <?php 
                                        if(isset($updatedDate) && $updatedDate != '0000-00-00 00:00:00'):
                                            echo __('lbl_updated').':&nbsp;'.sfGeneral::compareDates(
                                                strtotime($updatedDate),
                                                    __('msg_past_published'),
                                                    __('msg_past_published')
                                            );
                                            echo ",&nbsp;".date('g:i', strtotime($updatedDate)).__('lbl_gmt');
                                        endif;
                                    ?>
                                </span>
                            <?php endif; ?>
                        </td>
         </tr>
        <tr>
            <td align="left" valign="top">
                <br/>
                <span class="first_td metadataLabel">
                    <?php echo $advertisementForm['sponsored']->renderLabel();?>
                    <span class="redText">
                        <?php
                            if($advertisementForm['sponsored']->hasError()):
                                echo $advertisementForm['sponsored']->getError();
                            endif;
                        ?>
                    </span>
                </span>
                <br />
                <?php 
                    echo $advertisementForm['sponsored']->render(
                                                                array('class' => 'articleFormboxTextfilde metadataText',
                                                                'style'=>'width:48%;'));
                ?>
            </td>
        </tr>    

      </table>
    </div>
    <div class="addEditStoryContentBoxMain MT2" style="">
        <div class="addEditStoryContentBox" style=" min-height:<?php echo (count($advertisementForm->getErrorSchema()) != 0) ? '600px;' : ((count($advertisementForm->getErrorSchema()) == 0 && $sf_user->hasFlash('success_story')) ? '600px;' : '574px;');?>">
            <div class="">
                <?php
                     include_partial('addEditAdvertisement',
                        array(
                            'mediaObj'              => '',
                            'advertisementForm'     => $advertisementForm,
                            'idChannel'             => $idChannel,
                            'idAdvertisement'       => $idAdvertisement,
                            'updatedDate'           => (isset($advertiseObj['updated_at'])?$advertiseObj['updated_at']:''),
                            'idMedia'               => $idMedia,
                            'medias'                => isset($medias)?$medias:array(),
                            'mediaType'             => $mediaType,
                            'advertiseObj'          => isset($advertiseObj)?$advertiseObj:'',
                            'channels'              => $channels,
                            'channelAdvertisement'  => $channelAdvertisement,
                            'ssMode'                => isset($ssMode)?$ssMode:'',
                            'errorHandler'          => isset($errorHandler)?$errorHandler:''
                        )
                    );
                ?>
            </div>
        </div>
    </div>
    </form>
    </div>
</div>