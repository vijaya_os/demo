<?php
    use_helper('JavascriptBase','jQuery');
    $parameters = "id_advertisement=".$idAdvertisement;

    slot('first_update');
        include_partial('addEditTitle', 
                array(
                    'advertisementForm' => $advertisementForm,
                    'idAdvertisement'   => (($idAdvertisement)?$idAdvertisement:'')
                )
        );
    end_slot();
    
    echo javascript_tag(
        jq_update_element_function(
            'editAdvertisement', 
            array('content' => get_slot('first_update'))
        )
    );
    if($addAdvertisement && $sf_user->hasFlash('success_title')):
        slot('second_update');
?>
        <div id="editAdvertisementMenuDiv" onclick="showHideDetailPartion('editAdvertisement', false);
                showPageContent('editAdvertisement','<?php echo url_for('@add_edit_title');?>','<?php echo $parameters;?>');">
                    <?php echo __('lbl_edit_your_title'); ?>
                </div>
<?php   end_slot();
        slot('third_update');
?>
        <div id="editStoryMenuDiv" onclick="showHideDetailPartion('editStory', false);
        showPageContent('editStory','<?php echo url_for('@add_edit_advertisement');?>','<?php echo $parameters;?>');">
            <?php echo __('lbl_using_network'); ?>
        </div>
<?php   end_slot();

        slot('fourth_update');
        $parameter = $parameters.'&media_type=image';
?>
        <div id="uploadImageMenuDiv" onclick="showHideDetailPartion('uploadImage', false);
            showPageContent('uploadImage','<?php echo url_for('@upload_advertisement_media');?>',
            '<?php echo $parameter;?>');">
            <?php echo __('lbl_upload_your_image'); ?>
        </div>
<?php   end_slot();

        slot('fifth_update');
?>
        <div id="advertisementPlaceItMenuDiv" onclick="showHideDetailPartion('articlePlaceIt', false);
        showPageContent('articlePlaceIt',
                        '<?php echo url_for('@add_edit_advertisement');?>','<?php echo $parameters;?>');">
            <?php echo __('lbl_place_it'); ?>
        </div>
<?php   end_slot();



/*        slot('second_update');
            include_partial('advertisementBox', 
                array(
                    'advertisementForm' => $advertisementForm,
                    'advertisementObj'  => $advertisementObj,
                    'idAdvertisement'   => (($idAdvertisement)?$idAdvertisement:'')
                )
            );
        end_slot();
        
        echo javascript_tag(
            jq_update_element_function(
                'mainBox', 
                array('content' => get_slot('second_update'))
            ).'jQuery("#editAdvertiseMenu").attr("class", "blackBoxPop1")'
        );
  */
    echo javascript_tag('
        jQuery("#statusDiv").show();
        jQuery("#uploadImageMenu").show(); 
        jQuery("#editStoryMenu").show();
        jQuery("#advertisementPlaceItMenu").show();
        jQuery("#buttonDiv").show();'.
        jq_update_element_function('editAdvertisementMenu', array('content' => get_slot('second_update'))).
        jq_update_element_function('editStoryMenu', array('content' => get_slot('third_update'))).
        jq_update_element_function('uploadImageMenu', array('content' => get_slot('fourth_update'))).
        jq_update_element_function('advertisementPlaceItMenu', array('content' => get_slot('fifth_update')))
    );
  
  
  endif;
