<?php
    use_javascript('admin/jquery-1.4.1.min.js');
    use_helper('JavascriptBase', 'jQuery', 'Text');

    $statusSearch   = (isset($statusSearch) && $statusSearch == 'false') ? false : true;
    $searchValues   = array_keys($searchByArray);
    $permissionUrl  = url_for('@get_permissions?search=true');
?>

<div class="articlesearch">
    <div class="articlesearchFilde">
        <?php 
        echo input_tag('searchvalue',
            trim($sf_params->get('searchvalue')), array('class' => 'articlesearchFilde input', 'tabindex' => 1));
		echo input_hidden_tag('searchby',$searchValues[0]);
        ?>
    </div>
    <div class="articlesearch-BTN">
        <?php
            echo jq_submit_to_remote('search', '.',
                array(
                    'update'   => $update_div,
                    'url'      => $url,
                    'before'   => 'setOriginalPagePaging();',
                    'loading'  => jq_visual_effect('fadeIn','#indicator1'),
                    'complete' => jq_visual_effect('fadeOut','#indicator1')
                ),
                array(
                    'title' => __('tip_search'),
                    'class' => 'articlesearch-BTN input',
                    'id'    => 'search',
                    'tabindex' => 1
                )
            );
        ?>
    </div>
</div>

<div class="search-icon">
    <?php
        $perPageRecordsMsg = sprintf(__('tlp_show_per_page_records'), sfConfig::get('app_default_paging_records'));
        echo jq_link_to_remote(
            image_tag('search-1.jpg', array('border' => 0, 'alt' => $perPageRecordsMsg)),
            array(
                'update'   => $update_div,
                'url'      => $url,
                'before'   => 'flag = true; showAll("'.$searchValues[0].'");jQuery("#searchvalue").val("'.__('lbl_search_story').'")',
                'loading'  => jq_visual_effect('fadeIn','#indicator1'),
                'complete' => jq_visual_effect('fadeOut','#indicator1')
            ),
            array(
                'title' => $perPageRecordsMsg,
                'class' => '',
                'id'    => 'btnShowAll',
                'tabindex' => 1
            )
        );
    ?>
</div>
<div class="search-icon">
    <?php
        $perPageRecords = sfConfig::get('app_default_paging_records') * $maxPaging;
        $with           = "page=".sfConfig::get('app_default_page').'&updated_page='.sfConfig::get('app_default_page');
        $with           .= "&paging=$perPageRecords&updated_paging=$perPageRecords";

        echo jq_link_to_remote(
            image_tag('search-2.jpg', array('border' => 0, 'alt' => __('tlp_show_all'))),
            array(
                'method'   => 'POST',
                'update'   => $update_div,
                'with'      => "'$with'",
                'url'      => $url,
                'before'   => 'flag = true; jQuery("#updated_paging").val("All");jQuery("#searchvalue").val("'.__('lbl_search_story').'")',
                'loading'  => jq_visual_effect('fadeIn','#indicator1'),
                'complete' => jq_visual_effect('fadeOut','#indicator1')
            ),
            array(
                'title' => __('tlp_show_all'),
                'class' => '',
                'id'    => 'btnShowAll',
                'tabindex' => 1
            )
        );
    ?>
</div>

<?php
    echo javascript_tag('
        var flag = true;
        jQuery("#searchvalue").keydown(function (e) {
            if(e.keyCode == 13 && flag == true)
            {
                setOriginalPagePaging();
                jQuery("#go").click();
                flag = false;
            }
        });
        jQuery("#searchvalue").focus();
    ');
