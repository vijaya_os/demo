<?php
    $channels           = $sf_data->getRaw('channels');
    $channelAdvertisementList = $sf_data->getRaw('channelAdvertisementList');
    $errorHandler       = $sf_data->getRaw('errorHandler');
?>
<table width="100%" border="0" cellspacing="0" cellpadding="8">
    <tr>
        <td width="45%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                <tr>
                    <td width="32%" align="left" valign="top"></td>
                    <td width="68%" align="left" valign="top">&nbsp;</td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <span class="first_td">
                            <?php echo __('lbl_title');?>
                            <span class="redText">*</span>
                        </span>
                    </td>
                    <td width="75%" align="left" valign="top">
                        <?php 
                            $title   = ($sf_request->getMethod() == 'POST') ? 
                                            $sf_params->get('title') : $advertisementObj->getTitle();
                            echo input_tag(
                                'title', 
                                $title, 
                                array(
                                    'class'     => 'articleFormboxTextfilde', 
                                    'maxlength' => 30, 
                                    'style'     => 'width: 210px',
                                )
                            );
                            
                            if($errorHandler != '' && $errorHandler->hasError('title')):
                                echo '<br /><span class="redText">';
                                echo $errorHandler->getError('title');
                                echo '</span>';
                            endif;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top">
                        <span class="first_td"><?php echo __('lbl_upload_your_image');?></span>
                    </td>
                    <td width="75%" align="left" valign="top">
                        <?php 
                            echo input_file_tag('image'); 
                            
                            if($errorHandler != '' && $errorHandler->hasError('image')):
                                echo '<br /><span class="redText">';
                                echo $errorHandler->getError('image');
                                echo '</span>';
                            endif;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top">
                        <span class="first_td"><?php echo __('lbl_start_date');?></span>
                    </td>
                    <td width="75%" align="left" valign="top">
                        <?php 
                            $startDate  = $sf_params->get('start_date') ? 
                                            $sf_params->get('start_date') : 
                                            $advertisementObj->getStartDate();
                            echo input_tag(
                                'start_date', 
                                $startDate,
                                array(
                                    'class'     => 'articleFormboxTextfilde', 
                                    'maxlength' => 30, 
                                    'style'     => 'width: 210px',
                                    'readonly'  => true
                                )
                            );
                            
                            if($errorHandler != '' && $errorHandler->hasError('start_date')):
                                echo '<br /><span class="redText">';
                                echo $errorHandler->getError('start_date');
                                echo '</span>';
                            endif;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top">
                        <span class="first_td"><?php echo __('lbl_end_date');?></span>
                    </td>
                    <td width="75%" align="left" valign="top">
                        <?php 
                            $endDate    = $sf_params->get('end_date') ? 
                                            $sf_params->get('end_date') : 
                                            $advertisementObj->getEndDate();
                            echo input_tag(
                                'end_date', 
                                $endDate,
                                array(
                                    'class'     => 'articleFormboxTextfilde', 
                                    'maxlength' => 30, 
                                    'style'     => 'width: 210px',
                                    'readonly'  => true
                                )
                            );
                            
                            if($errorHandler != '' && $errorHandler->hasError('end_date')):
                                echo '<br /><span class="redText">';
                                echo $errorHandler->getError('end_date');
                                echo '</span>';
                            endif;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top">
                        <span class="first_td"><?php echo __('lbl_status');?></span>
                    </td>
                    <td width="75%" align="left" valign="top">
                        <?php 
                            $status = $sf_params->get('status') ?
                                $sf_params->get('status') : $advertisementObj->getStatus();
                            
                            echo select_tag(
                                'status', 
                                options_for_select(
                                    sfConfig::get('app_advertisement_status'),
                                    $status
                                ), 
                                array(
                                    'class'     => 'articleFormboxSelectfilde', 
                                    'style'     => 'width: 222px; background-color:#FFFFFF;',
                                    'readonly'  => true
                                )
                            );
                        ?>
                    </td>
                </tr>
                 <tr>
                    <td align="left" valign="top">&nbsp;</td>
                    <td align="left" valign="top" colspan="2">
                        <div class="sourceAdd">
                            <?php
                                echo submit_tag(
                                    __('btn_submit'), 
                                    array(
                                        'class'     => 'submitButInput', 
                                        'name'      => 'submit_button', 
                                        'title'     => __('btn_submit'), 
                                        'tabindex'  => 1,
                                    )
                                );
                            ?>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        
        <td width="25%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                <tr>
                    <td width="100%" align="left" valign="top">
                        <div class="boxW2MidTopNorma2">
                            <strong><?php echo __('lbl_place_your_advertisement_on_channels');?></strong>
                        </div>
                        <div class="placeYourStory" style="height:200px;">
                            <?php foreach($channels as $key => $channel): ?>
                                <div class="boxW2MidTopNorma2">
                                    <?php echo checkbox_tag('id_channels[]', $channel['id'], 
                                    ((in_array($channel['id'], $channelAdvertisementList)) ? true : false),array('class'=>'checkBoxWidth')); ?>
                                    &nbsp;<?php echo $channel['channel_name']; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php
                            if($errorHandler != '' && $errorHandler->hasError('place_it')):
                                echo '<span class="redText">';
                                echo $errorHandler->getError('place_it');
                                echo '</span>';
                            endif;
                        ?>
                    </td>
                </tr>
            </table>
        </td>
        
        <td width="30%" valign="top"> &nbsp; </td>
        
    </tr>
    
</table>