<?php 
    include_partial('uploadMedia.js');
    switch($mediaType)
    {
        case strtolower(sfConfig::get('app_media_main_type_image')):
            $maxFileLimit   = sfConfig::get('app_max_image_upload_limit');
            $messages       = __('err_invalid_image').'|'.sprintf(__('msg_maximum_upload_image_size'), 
                            sfConfig::get('app_max_image_upload_limit'));
            $fileType       = strtolower(sfConfig::get('app_media_main_type_image'));
            $uploadSizeText = sprintf(
                __('txt_upload_image'), 
                sfConfig::get('app_max_image_upload_limit'), 
                sfConfig::get('app_thumb_big_image_width'), 
                sfConfig::get('app_thumb_big_image_height')
            );
            $fileSource     = sprintf(sfConfig::get('app_upload_path_advertisement_image'), $idAdvertisement);
            $uploadFile     = __('lbl_upload_image');
            $baseElementId   = 'newFileImage';
            break;
    }

    $url        = '@upload_advertisement_media?update='.strtolower($sf_params->get('media_type'));
    $url        .= '&method=POST&id_advertisement='.$idAdvertisement;
    $url        = url_for($url);
    
    echo javascript_tag("
        jQuery(document).ready(function() {
            createUploader('".$fileType."', ".$maxFileLimit.", '".$fileSource."', '".$url."', '".$messages."','".
                              $baseElementId."');
        });
    ");
    
    $idAdvertisement  = $sf_params->get('id_advertisement');
    
    echo input_hidden_tag('id_advertisement', $idAdvertisement, array('readonly' => true));
?>

<!--    upload media    -->
<?php if($sf_user->hasFlash('success_media')): ?>
    <div id="successClass" style="width:100%;">
        <div class="success canhide"><?php echo $sf_user->getFlash('success_media'); ?></div>
    </div>
<?php else: ?>
    <div id="successClass" style="width:100%; display:none;">
        <div class="success canhide"><?php echo __('msg_file_uploaded'); ?></div>
    </div>
<?php endif; ?>

<div id="errorClass" style="width:100%; display:none;">
    <div id="errorMsg" class="fail canhide">&nbsp;</div>
</div>

<div class="boxW2">
    <div class="boxW2TiSub"><?php echo $uploadFile; ?><div class="boxDArrowRight"></div></div>
   
    <div class="boxW2Sub ">
       <div class="boxW2DropText"><?php echo __('lbl_drag_and_drop'); ?><br />or</div>
       <div class="boxW2Select"><span id="<?php echo $baseElementId; ?>"></span></div>
    </div>
    
    <div class="boxW2Midd">
        <div class="boxW2MidTop">
            <div class="boxW2MidTopNormal"><?php echo $uploadSizeText; ?></div>
        </div>
    </div>
    
    <div class="seemoreLink" id="seemore" style="display:none;"></div>    
    <div style="color:red; display:none;" id="errorMsg">&nbsp;</div>
    <div id="addEditMediaMetadata"></div>
</div>
<!--    end of upload media    -->
