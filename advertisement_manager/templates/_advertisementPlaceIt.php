<div>
<?php
    $errorHandler       = $sf_data->getRaw('errorHandler');
    $channelArticleList = array();
    foreach($channelArticles as $channelArticle):
        $channelArticleList[]   = $channelArticle['channel_id'];
    endforeach;

    if($errorHandler->getErrorCount() == 0 && $sf_user->hasFlash('success_channels')): ?>
    <div id="errorClass">
        <div class="success canhide"><?php echo $sf_user->getFlash('success_channels'); ?></div>
    </div>
<?php endif;
    if($errorHandler != '' && $errorHandler->hasError('place_it')):
        echo '<span class="redText">';
        echo $errorHandler->getError('place_it');
        echo '</span>';
    endif;
?>
    <div class="boxW2" style="overflow:auto;height:250px;">
        <div class="boxW2MidTopNorma2">
                <?php
                    echo ($mediaType == strtolower(sfConfig::get('app_media_main_type_image'))) ?
                        __('lbl_place_your_story_on_channels') : __('lbl_place_your_song_on_channels');
                ?>
        </div>
        <div >
            <?php foreach($channels as $key => $channel): ?>
                <div class="boxW2MidTopNorma2">
                    <?php echo checkbox_tag('id_channels[]', $channel['id'],
                    ((in_array($channel['id'], $channelArticleList)) ? true : false),array('class'=>'checkBoxWidth')); ?>
                    &nbsp;<span class="font13"><?php echo $channel['channel_name']; ?></span>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="clear placeITBorderBottom">&nbsp;</div>
    </div>
</div>