<?php echo use_helper('JavascriptBase', 'jQuery','Date'); ?>
<div class="editStory">
    <div class="editStoryTable">
     <?php
        if(count($advertisementForm->getErrorSchema()) == 0 && $sf_user->hasFlash('success_story')):
            include_partial(
                'global/successErrorMessage',
                array(
                    'successMessage' => $sf_user->getFlash('success_story')
                )
            );
            if($ssMode == 'Draft'){
                echo javascript_tag("openFlashMessage('".__('msg_flash_story_draft')."',false)");
            }else{
                echo javascript_tag("openFlashMessage('".__('msg_flash_story_published')."',false)");
            }
            
            $uploadMediaUrl = url_for('@upload_media?id_channel='.$idChannel);
        endif;
    ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MT5">
            	<tr>
                	<td width="50%" valign="top">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="addEditStoryTableL">
                            <tr>
                                <td align="left" valign="top">
                                    <span class="first_td metadataLabel">
                                        <?php echo $advertisementForm['title']->renderLabel();?>
                                        <span class="redText">
                                            <?php
                                                if($advertisementForm['title']->hasError()):
                                                    echo $advertisementForm['title']->getError();
                                                endif;
                                            ?>
                                        </span>
                                    </span>
                                    <br />
                                    <?php 
                                        echo $advertisementForm['title']->render(
                                                                                    array('class' => 'articleFormboxTextfilde metadataText'));
                                    ?>
                                </td>
                            </tr>    
                            <tr>
                                <td align="left" valign="top">
                                    <span class="first_td storyLabel" style="padding-top:10px">
                                        <?php echo $advertisementForm['readermode_text']->renderLabel();?>
                                        <span class="redText">
                                        <?php
                                        if($advertisementForm['readermode_text']->hasError()):
                                            echo $advertisementForm['readermode_text']->getError();
                                        endif;
                                         ?>
                                         </span>
                                    </span><br />
                                    <?php 
                                        echo $advertisementForm['readermode_text']->render(array());
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <span class="first_td storyLabel" style="padding-top:10px">
                                        <?php echo $advertisementForm['target_link']->renderLabel();?>
                                        <span class="redText">
                                        <?php 
                                        if($advertisementForm['target_link']->hasError()):
                                            echo $advertisementForm['target_link']->getError();
                                        endif;?>
                                        </span>
                                    </span><br/>
                                    <?php 
                                        echo $advertisementForm['target_link']->render(array('class' => 'articleFormboxTextfilde'));
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" class="startDateStories">
                                	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="storyDateBox">
                                    	<tr>
                                        	<td width="50%">
                                                <span class="first_td storyLabel" style="padding-top:10px"><label><?php echo __('lbl_start_date');?></label></span>
                                                <br/>
                                                <?php 
                                                    $startDate   = ($sf_request->getMethod() == 'POST') ?  
                                                                            $sf_params->get('start_date') :
                                                                            (( $advertiseObj && $advertiseObj->getStartDate() != '' && 
                                                                                $advertiseObj->getStartDate() != '0000-00-00 00:00:00') ? 
                                                                                $advertiseObj->getStartDate() : 
                                                                                date('m/d/y')
                                                                            );
                                                    $startYear  = date('y', strtotime($startDate));
                                                    $startMonth = date('m', strtotime($startDate));
                                                    $startDay   = date('d', strtotime($startDate));
                                                    
                                                    echo input_tag(
                                                        'publish_start_month', 
                                                        $startMonth, 
                                                        array('tabindex' => 1, 'size' => '1px','class'=>'first', 'readonly' => true)
                                                    ).' / ';
                                                    echo input_tag(
                                                        'publish_start_day', 
                                                        $startDay, 
                                                        array('tabindex' => 1, 'size' => '1px', 'readonly' => true)
                                                    ).' / ';
                                                    echo input_tag(
                                                        'publish_start_year', 
                                                        $startYear, 
                                                        array('tabindex' => 1, 'size' => '1px', 'readonly' => true)
                                                    ); 
                                            
                                                    echo input_hidden_tag('start_date', $startDate, array('readonly' => true));
                                                    echo input_hidden_tag('datepicker_start', '', array('tabindex' => 1, 'readonly' => true));
                                            
                                                    if($errorHandler != '' && $errorHandler->hasError('publish_start_date')):
                                                        echo '<br /><span class="redText">';
                                                        echo $errorHandler->getError('publish_start_date');
                                                        echo '</span>';
                                                    endif;
                                                ?>
                                            </td>
                                            <td width="50%">
                                            	<span class="first_td  storyLabel" style="padding-top:10px"><label><?php echo __('lbl_end_date');?></label></span>
                                                <br/>
                                                <?php 
                                                    $endDate = ($sf_request->getMethod() == 'POST') ?  
                                                                            $sf_params->get('end_date') : 
                                                                            (($advertiseObj && $advertiseObj->getEndDate() != '' && 
                                                                                $advertiseObj->getEndDate() != '0000-00-00 00:00:00') ? 
                                                                                $advertiseObj->getEndDate() : 
                                                                                date('m/d/y')
                                                                            );
                                                                                        
                                                    $endYear  = date('y', strtotime($endDate));
                                                    $endMonth = date('m', strtotime($endDate));
                                                    $endDay   = date('d', strtotime($endDate));
                                                    
                                                    echo input_tag(
                                                        'publish_end_month', 
                                                        $endMonth, 
                                                        array('tabindex' => 1, 'size' => '1px','class'=>'first', 'readonly' => true)
                                                    ).' / ';
                                                    echo input_tag(
                                                        'publish_end_day', 
                                                        $endDay, 
                                                        array('tabindex' => 1, 'size' => '1px', 'readonly' => true)
                                                    ).' / ';
                                                    echo input_tag(
                                                        'publish_end_year', 
                                                        $endYear, 
                                                        array('tabindex' => 1, 'size' => '1px', 'readonly' => true)
                                                    ); 
                                            
                                                    echo input_hidden_tag('end_date', $endDate, array('readonly' => true));
                                                    echo input_hidden_tag('datepicker_end', '', array('tabindex' => 1, 'readonly' => true));
                                            
                                                    if($errorHandler != '' && $errorHandler->hasError('publish_end_date')):
                                                        echo '<br /><span class="redText">';
                                                        echo $errorHandler->getError('publish_end_date');
                                                        echo '</span>';
                                                    endif;
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td align="right" valign="top" class="PTB10">
                            	<div class="clear" style=" min-height:<?php echo (count($advertisementForm->getErrorSchema()) != 0) ? '190px;' : ((count($advertisementForm->getErrorSchema()) == 0 && $sf_user->hasFlash('success_story')) ? '165px;' : '165px;');?>">&nbsp;</div>
                            </td></tr>
                            <tr>
                            	<td align="right" valign="top" class="PTB10">
                                	<div>
                            			<div class="publish fright PR0L12">
                                            <div class="publishBTN">
                                                <?php
                                                    $options    = array(
                                                        'method'    => 'POST',
                                                        'update'    => 'addEditAdvertisement',
                                                        'url'       => url_for('@add_edit_advertise'),                                
                                                        'loading'   => jq_visual_effect('fadeIn','#createStoryInd').
                                                                        "jQuery('#successClass, .success').hide();",
                                                        'before'    => 'replaceEditorValAdvertisement();',
                                                        'complete'  => jq_visual_effect('fadeOut','#createStoryInd'),
                                                        'success'   => 'jQuery("#addEditAdvertisement").html(data)'
                                                        );
                                                
                                                    $htmlOptions   = array(
                                                        'title'     => __('btn_publish'), 
                                                        'id'        => 'saveEditAdvertisement',
                                                        'class'     => '',
                                                        'tabindex'  => 1
                                                     );
                            
                                                    echo jq_submit_to_remote(
                                                        'saveEditAdvertisement', 
                                                        __('btn_publish'), 
                                                        $options, 
                                                        $htmlOptions
                                                    );
                                                ?>
                                             </div>
                                    	 </div>
                                        <div class="draft fright">
                                            <div class="draftBTN">
                                                <?php
                                                    $options    = array(
                                                        'method'    => 'POST',
                                                        'update'    => 'addEditAdvertisement',
                                                        'url'       => url_for('@add_edit_advertise?mode=Draft'),                                
                                                        'loading'   => jq_visual_effect('fadeIn','#createStoryInd').
                                                                        "jQuery('#successClass, .success').hide();",
                                                        'before'    => 'replaceEditorValAdvertisement()',
                                                        'complete'  => jq_visual_effect('fadeOut','#createStoryInd'),
                                                        'success'   => 'jQuery("#addEditAdvertisement").html(data)'
                                                    );
                                                
                                                    $htmlOptions   = array(
                                                        'title'     => __('tip_draft'), 
                                                        'id'        => 'saveEditAdvertisement',
                                                        'class'     => '',
                                                        'tabindex'  => 1
                                                     );
                            
                                                    echo jq_submit_to_remote(
                                                        'saveEditAdvertisement', 
                                                        __('btn_draft'), 
                                                        $options, 
                                                        $htmlOptions
                                                    );
                                                ?>
                                             </div>
                                         </div>
                                        <span class="cancelLinkBtn">
                                            <span class="cancelLinkBtnLeft">
                                                 <?php
                                                    echo link_to_function(
                                                        __('lnk_btn_cancel'),
                                                        'window.location.reload(true)',
                                                        array(
                                                            'title'     => __('lnk_btn_cancel'),
                                                            'tabindex'  => 1
                                                        )
                                                    );
                                                ?>
                                            </span>
                                        </span>
                        			</div>
                    			</td>
                            </tr>
                        </table>
                    </td>
                    <td align="left" valign="top" width="50%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="addEditStoryTableR">
                            <tr>
                                <td>
                                    <?php
                                    $sf_user->setFlash('error_story',''); 
                                       include_partial('uploadMedia',
                                           array('idAdvertisement'=> $idAdvertisement,
                                               'idChannel'  => $idChannel,
                                               'idMedia'    => $idMedia,
                                               'medias'     => $medias,
                                               'mediaType'  => $mediaType,
                                               'advertiseObj' => $advertiseObj,
                                               'errorHandler'          => isset($errorHandler) ? $errorHandler : '',
                                           )
                                        );
                                       
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php
                                    $sf_user->setFlash('error_story',''); 
                                    include_partial('advertisePlaceIt', 
                                        array(
                                            'idAdvertisement'       => $idAdvertisement, 
                                            'idChannel'             => $idChannel,
                                            'channels'              => $channels, 
                                            'channelAdvertisement'  => $channelAdvertisement, 
                                            'advertiseObj'          => isset($advertiseObj)?$advertiseObj:'',
                                            'mediaType'             => isset($mediaType) ? $mediaType : '',
                                            'errorHandler'          => isset($errorHandler) ? $errorHandler : '',
											'advertisementForm' 	=> $advertisementForm
                                        )
                                    );
                                ?>
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
            </table>
    </div> 
</div>