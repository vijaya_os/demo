<?php
    use_helper('Form', 'jQuery', 'JavascriptBase', 'I18N');

    $channelAdvertisementList = array();
    if(count($channelAdvertisements) > 0):
        foreach($channelAdvertisements as $channelAdvertisement):
            $channelAdvertisementList[]   = $channelAdvertisement['channel_id'];
        endforeach;
    endif;
    
    $errorHandler   = isset($errorHandler) ? $errorHandler : '';
?>

<div class="editStory" style="width: 95%">
    <?php if(($sf_request->isMethod('POST') && $errorHandler->getErrorCount() == 0) && 
        $sf_user->hasFlash('success_quick_edit')): ?>
        <div class="success canhide"><?php echo $sf_user->getFlash('success_quick_edit'); ?></div>
    <?php endif; ?>
    
    <div class="articleFormboxForm" style="width: 100%">
        <?php
            echo include_partial('global/indicator');
             
            echo form_tag(
                '@quick_edit_advertisement', 
                array('name' => 'frm_article', 'enctype' => 'multipart/form-data')
            );
            
            echo input_hidden_tag('id_advertisement', $idAdvertisement, array('readonly' => true));
            echo input_hidden_tag('id_media', $idMedia, array('readonly' => true));
            echo input_hidden_tag('loaderImage', 'loader.gif', array('readonly' => true));
            echo input_hidden_tag('page', $page, array('readonly' => true));
            echo input_hidden_tag('paging', $paging, array('readonly' => true));
            echo input_hidden_tag('actual_page', $actualPage, array('readonly' => true));
            echo input_hidden_tag('actual_paging', $actualPaging, array('readonly' => true));
            echo input_hidden_tag('updated_page', $updatedPage, array('readonly' => true));
            echo input_hidden_tag('updated_paging', $updatedPaging, array('readonly' => true));
            echo input_hidden_tag('set_paging', false, array('readonly' => true));
            
            echo '<div style="width: 1024px;" id="updateQuickEdit">';
                include_partial(
                    'quickEdit',
                    array(
                        'advertisementObj'          => $advertisementObj,
                        'channels'                  => $channels,
                        'channelAdvertisementList'  => $channelAdvertisementList,
                        'errorHandler'              => $errorHandler
                    )
                );
            echo '</div>';            
        ?>
        </form>
    </div> 
    <div class="divclear"></div>           
</div>
<?php
    if($sf_request->getMethod() == 'POST'):
        include_javascripts('admin/jquery.min.js');
        include_javascripts('admin/jquery-1.4.1.min.js');

        include_javascripts('timepicker/jquery-ui-1.8.14.custom.min.js');
        include_javascripts('timepicker/jquery-ui-timepicker.js');
        
        echo javascript_tag("setTimeout('dateTimePicker()', 2000);");
    else:
        echo javascript_tag("setTimeout('dateTimePicker()', 1000);"); 
    endif;

    echo javascript_tag("
        function dateTimePicker()
        {
            jQuery('#start_date, #end_date').datetimepicker({dateFormat:'yy-mm-dd',timeFormat: 'hh:mm:ss'});
        }
    ");
