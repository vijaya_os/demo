<?php 
echo input_hidden_tag('sortby',$sortby, array('readonly' => true));
echo input_hidden_tag('sortmode',$sortmode, array('readonly' => true));
echo input_hidden_tag('inactivateIds', '', array('readonly' => true));
echo input_hidden_tag('extraParameters',$extraParameters['querystr'], array('readonly' => true));
echo input_hidden_tag('total_records', $listObj->getNbResults(), array('readonly' => true));
    
if($listObj->getNbResults() > 0):
    echo input_hidden_tag('admin_act', '', array('readonly' => true));
?>

    <div class="articleBox">
        <ul id="middle_content">
            <li class="grayN Hbar">
                <div class="selectAllMain" style="width:17%" padding-right:35px;>
                    <?php 
                        include_partial('global/top_action', 
                            array(
                                'id_checkboxes'         => $id_checkboxes,
                                'update_div'            => 'success_msgs',
                                'url'                   => '@manage_advertisements?request_type=ajax_request',
                                'totalCount'            => $totalCount,
                                'admin_act_module'      => 'delete',
                                'deleteButton'          => true,
                                'showCheckAll'          => true,
                                'showPopupLayout'       => false,
                                'form_name'             => $form_name,
                                'inactivateIds'         => $inactivateIds
                            )
                        ); 
                    ?>
                </div>
                
                 
                
                <div class="articleBoxHeadingDate" style="width:25%">
                    <div class="heading_bold"><?php echo __('lbl_advertisement')?></div>
                    <div id="field_div_headline" class="articleBoxHeadingDateIMG">
                        <?php 
                            include_partial('global/sort_ajaxmain',
                                array(
                                    'sortBy'    => 'title',
                                    'url'       => $url,
                                    'updateDiv' => 'contentlisting'
                                )
                            ); 
                        ?>
                    </div>
                </div>
                
                <div class="articleBoxHeadingDate" style="width:20%">
                    <div class="heading_bold"><?php echo __('lbl_channels')?></div>
                </div>
				
				<div class="articleBoxHeadingDate" style="width:24%">
                    <div class="heading_bold"><?php echo __('lbl_date')?></div>
                </div>
				
                <div class="articleBoxHeadingDate" style="width:10%">
                    <div class="heading_bold"><?php echo __('lbl_placement')?></div>
                </div>
                
                 
            </li>
            <?php 
                include_partial('list_middle_content', 
                    array(
                        'listRecords' => $listRecords, 
                        'extraParameters' => $extraParameters, 
                        'url' => '@manage_advertisements?request_type=ajax_request'
                    )
                );
            ?>
        </ul>
    </div>
<?php 
    else:
        echo '<div class="attention canhide">'.__('lbl_no_record').'</div>';
    endif; 
?>
