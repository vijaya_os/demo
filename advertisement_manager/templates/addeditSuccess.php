<?php
    use_helper('JavascriptBase', 'jQuery');
    echo include_partial('global/indicator');
?>

<div id="wapper" class="fleft">
    <div id="advetisementMain">
    
        <!--    main box    -->
        <div id="mainBox">
            <?php
                include_partial(
                    'advertisementBox',
                    array(
                        'advertisementObj'  => $advertisementObj, 
                        'idAdvertisement'   => ($idAdvertisement) ? $idAdvertisement : '', 
                    )
                );
            ?>
        </div>
        <!--    end of main box    -->
        <!--    edit headlines -->
        <div class="boxW2Main" id="editAdvertisement" style="display:none;"></div>
        <!--    end of edit headlines -->

        <!--    crop image -->
        <div class="boxW2Main" id="connectNetwork" style="display:none;"></div>
        <!--    end of crop image -->

        <!--    upload image    -->
        <div class="boxW2Main" id="uploadImage" style="display:none;"></div>
        <!--    end of upload image -->

        <!--    place it -->
        <div class="boxW2Main" id="advertisementPlaceIt" style="display:none;"></div>
        <!--    end of place it -->   
    </div>
</div>