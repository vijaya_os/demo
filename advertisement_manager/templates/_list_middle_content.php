<?php
 $rollList              = sfConfig::get('app_adroll');
 foreach($sf_data->getRaw('listRecords') as $listKey => $listRecord):
        $idAdvertisement        = $listRecord['id'];
        $advertisementMedias    = $listRecord['AdvertisementMedia'];
		
        $idMedia                = '';
          
        if(count($advertisementMedias) > 0):
            foreach($advertisementMedias as $advertisementMedia):
                if((isset($advertisementMedia['Media']) && is_array($advertisementMedia['Media'])) && $advertisementMedia['Media']['main_type'] == 'Image'):
                    $path       = $advertisementMedia['Media']['path'];
                    $idMedia    = $advertisementMedia['Media']['id'];
                    break;
                elseif((isset($advertisementMedia['Media']) && is_array($advertisementMedia['Media'])) && $advertisementMedia['Media']['main_type'] == 'Video'):
                    $path       = $advertisementMedia['Media']['path'];
                    $idMedia    = $advertisementMedia['Media']['id'];
                    break;
                endif;
            endforeach;
        endif;
    
    ?>
    <li>
        <div class="box_normal_text" style="width:14%; padding-right:35px;height:77px;">
            <?php 
                echo checkbox_tag(
                    'id[]', 
                    $idAdvertisement, 
                    '', 
                    array(
                        'onclick'   => 'singleCheck(this,"id[]","idAdvertisement")',
                        'class'     => 'checkB'
                    )
                ); 
            ?>
    
            <?php
            
                $image  = (isset($path) && 
                    file_exists(
                        sprintf(
                            sfConfig::get('sf_web_dir').sfConfig::get('app_view_path_advertisement_small_image'), 
                            $idAdvertisement, 
                            $path
                        )
                    )
                ) ?
                sprintf(
                    sfConfig::get('app_view_path_advertisement_small_image'),
                    $idAdvertisement, 
                    $path
                ) : 
                'noimage-small.png';

                echo image_tag(
                        $image, 
                        array(
                           'class' => 'image0 imgB', 
                           'id'    => 'image_'.$idAdvertisement
                        )
                    );
            ?>
        </div>
        <div class="boxHeadline" style="width:25%;">
            <div class="box_normal_text"><?php echo $listRecord['title'] ? '['.$listRecord['title'].']' : ''; ?></div>
            <div class="BoxQuick">
                <ul>
                    <li>
                        <?php
							$editAdvertiseUrl   = url_for('@add_edit_advertise?media_type=image');
                            echo link_to_function(
                                __('lnk_edit'),
                                "getEditAdvertiseData('".$editAdvertiseUrl."', ".$idAdvertisement.", 'create_advertisement_div')",
                                array(
                                    'title' => __('lnk_edit'),
                                    'id'    => 'lnkCreateAdvertise'
                                )
                            );
                        ?>
                    </li>
                    <li>|</li>
                    <li>
                        <?php 
                            $url    = '@quick_edit_advertisement?id_advertisement='.$idAdvertisement;
                            $url    .= $idMedia ? '&id_media='.$idMedia : '';
                            $queryString    = html_entity_decode($extraParameters['querystr']);
                            echo link_to(
                                __('lnk_quick_edit'), 
                                url_for($url.'&'.$queryString), 
                                array('title' => __('lnk_quick_edit'))
                            );
                        ?>
                    </li>
                    <li>|</li>
                    <li>
                        <?php
                            $url         = '@manage_advertisements?id_advertisement='.$idAdvertisement.'&admin_act=delete';
                            $url         .= '&request_type=ajax_request';
                            $url         .= '&'.html_entity_decode($extraParameters['querystr']);
                            $deleteUrl   = url_for($url);
                            echo jq_link_to_function(
                                __('lnk_delete'), 
                                'if(confirm("'.__('msg_sure_to_delete').'")){
                                    deleteRecord("'.$idAdvertisement.'","'.$deleteUrl.'","contentlisting","delete_one")
                                }', 
                                array('title' => __('lnk_delete'), 'style' => 'cursor:pointer')
                            ); 
                        ?>
                    </li>
                </ul>
            </div>
            <div class="box_normal_text"><?php echo strtoupper($listRecord['status']); ?></div>
        </div>
        <div class="box_normal_text" style="width: 20%">
            <?php 
                if($listRecord['channel_name']):
                    $channelsName   = explode(',', $listRecord['channel_name']);
                    sort($channelsName);
                    echo '- '.implode('<br>- ', $channelsName);
                else:
                    echo '-';
                endif;
            ?>
        </div>
        <div class="box_normal_text" style="width: 24%">
            <?php echo ($listRecord['start_date'] == '0000-00-00') ? '-' : $listRecord['start_date']; ?>
            <br/>
            <?php echo ($listRecord['end_date'] == '0000-00-00') ? '-' : $listRecord['end_date']; ?>
        </div>
        
        <div class="box_normal_text" style="width: 10%">
			<?php 
				if(isset($listRecord['placement'])):
					$tempArray = explode(',',$listRecord['placement']);
					$prepareRoll = array();
					foreach($tempArray as $arr):
							$prepareRoll[] = $rollList[$arr];
					endforeach;
					echo '- '.implode('<br>- ', $prepareRoll);
				else:
                    echo '-';				
				endif;
			?>
		</div>
        <div class="divclear"></div>
    </li>
<?php endforeach; ?>