<?php 
use_helper('JavascriptBase','jQuery' , 'pagination');

$moduleName = $sf_params->get('module');
slot('first_update');
    include_partial(
        'list_middle_part',
        array(
            'listObj'           => $listObj,
            'listRecords'       => $listRecords,
            'extraParameters'   => $extraParameters,
            'moduleName'        => $moduleName,
            'sortby'            => $extraParameters['sortBy'],
            'sortmode'          => $extraParameters['sortMode'],
            'id_checkboxes'     => 'id[]',
            'form_name'         => $formName,
            'update_div'        => 'success_msgs',
            'url'               => '@manage_advertisements?request_type=ajax_request',
            'totalCount'        => $pageTotalRecords,
            'admin_act_module'  => 'delete',
            'deleteButton'      => true,
            'showCheckAll'      => true,
            'showPopupLayout'   => false,
            'inactivateIds'     =>'idUser',
            'userRole'          => '',
            'id'                => '' 
        )
    );
end_slot();

slot('second_update');
    include_partial(
        'global/paging', 
        array(
            'pager'         => $listObj,
            'url'           => '@manage_advertisements',
            'apendDiv'      => 'middle_content', 
            'totalCount'    => $listObj->getNbResults(),
            'label'         => __('lnk_click_here_to_view_more_advertisements')
        )
    );
end_slot();

slot('third_update');
    include_partial(
        'global/notification_msg', 
        array(
            'successMsg' => $successMsg,
            'errorMsg' => $errorMsg
        )
    );
end_slot();

slot('forth_update');
    include_partial(
        'global/total_items',
        array(
            'pager' => $listObj
        )
    );
end_slot();

echo javascript_tag(
    jq_update_element_function(
        'contentlisting', 
        array('content' => get_slot('first_update'))
    ).
    jq_update_element_function(
        'paging_div', 
        array(
            'content' => get_slot('second_update'),
        )
    ).
    jq_update_element_function(
        'success_msgs', 
        array(
            'content' => get_slot('third_update'),
        )
    ).
    jq_update_element_function(
        'total_items', 
        array(
            'content' => get_slot('forth_update'),
        )
    )
);

