<?php
    use_helper('pagination', 'jQuery', 'JavascriptBase', 'I18N');

    $moduleName     = $sf_params->get('module');
    $listRecords    = $sf_data->getRaw('listRecords');
    
    use_javascript("cropimage/jquery.Jcrop.js");
    use_stylesheet("cropimage/jquery.Jcrop.css");
    
    use_javascript('ckeditor/ckeditor.js');

    //  dialog
    use_stylesheet('dialog/jquery-ui-1.8.20.custom.css');
    use_javascript('dialog/jquery-1.7.2.min.js');
    use_javascript('dialog/jquery-ui-1.8.20.custom.min.js');
    //  end of dialog   

    use_javascript('timepicker/jquery-ui-1.8.14.custom.min.js');
    use_javascript('timepicker/jquery-ui-timepicker.js');
    use_stylesheet('timepicker/timepicker.css');
?>

<div class="articlecontent-mid" style="width:90%">
    <div class="Manageyour-ti"><?php echo __('lbl_manage_your_advertisements') ?></div>
    
    <div id="success_msgs">
        <?php echo include_partial('global/notification_msg',
                array(
                    'succ_msg' => $sf_user->getFlash('succ_msg'),
                    'errorMsg' => $errorMsg
                )
            );
        ?>
    </div>
    
    <?php
        echo include_partial('global/indicator');
        
        echo jq_form_remote_tag(
            array(
                'url'      => '@manage_advertisements?request_type=ajax_request',
                'update'   => 'contentlisting',
                'script'   => true,
                'loading'  => jq_visual_effect('fadeIn','#indicator1'),
                'complete' => jq_visual_effect('fadeOut','#indicator1')
            ),
            array(
                'name' => $formName, 'id' => $formName
            )
        );
        
        echo input_hidden_tag('loaderImage', 'loader.gif', array('readonly' => true));
        echo input_hidden_tag('page', $page, array('readonly' => true));
        echo input_hidden_tag('paging', $paging, array('readonly' => true));
        echo input_hidden_tag('actual_page', $actualPage, array('readonly' => true));
        echo input_hidden_tag('actual_paging', $actualPaging, array('readonly' => true));
        echo input_hidden_tag('updated_page', $updatedPage, array('readonly' => true));
        echo input_hidden_tag('updated_paging', $updatedPaging, array('readonly' => true));
        echo input_hidden_tag('set_paging', false, array('readonly' => true));
        echo input_hidden_tag('form_name', $formName, array('readonly' => true));
    ?>
    
    <!--    search partial  -->
    <div class="articlesearchMain">
        <?php
            include_partial(
                'search',
                array(
                    'url'                       => '@manage_advertisements?request_type=ajax_request',
                    'update_div'                => 'contentlisting',
                    'searchByArray'             => $search,
                    'statusSearchActiveInactive'=> false,
                    'page'                      => $page,
                    'maxPaging'                 => $maxPaging
                )
            );
        ?>
    </div>
    <!--    search partial  -->
    
    <!--    total displaying records   -->      
    <div class="articleDateMain" id="total_items">
        <?php include_partial('global/total_items', array('pager' => $listObj)); ?>
    </div>
    <!--    end of total displaying records   -->

                
    <div class="articleBody">
        <!--    header portion of listing   -->
        <?php if($pageTotalRecords == 0): ?>
            <div class="articleBoxHeading">
                <div class='attention canhide'><?php echo __('lbl_no_record'); ?></div>
            </div>
        <?php else: ?>
            <div class="articleBoxHeading" id="contentlisting">
                <?php
                    include_partial('list_middle_part',
                        array(
                            'listObj'           => $listObj,
                            'listRecords'       => $listRecords,
                            'extraParameters'   => $extraParameters,
                            'moduleName'        => $moduleName,
                            'sortby'            => $extraParameters['sortBy'],
                            'sortmode'          => $extraParameters['sortMode'],
                            'id_checkboxes'     => 'id[]',
                            'form_name'         => $formName,
                            'update_div'        => 'success_msgs',
                            'url'               => '@manage_advertisements?request_type=ajax_request',
                            'totalCount'        => $pageTotalRecords,
                            'admin_act_module'  => 'delete',
                            'deleteButton'      => true,
                            'showCheckAll'      => true,
                            'showPopupLayout'   => false,
                            'inactivateIds'     =>'idUser',
                            'userRole'          => '',
                            'id'                => '' 
                        )
                    );

                ?>
            </div>
            
            <div id="paging_div">
                <?php
                    if($listObj->haveToPaginate()):
                        include_partial('global/paging', 
                            array(
                                'pager'         => $listObj, 
                                'url'           => '@manage_advertisements', 
                                'apendDiv'      => 'middle_content', 
                                'totalCount'    => $listObj->getNbResults(),
                                'label'         => __('lnk_click_here_to_view_more_advertisements')
                            )
                        );
                    endif;
                ?>
            </div>
            
        <?php endif; ?>
        <!--    end of header portion of listing   -->
    </div>
</div>