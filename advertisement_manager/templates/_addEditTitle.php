<div class="editStory">
    <?php
     if($advertisementForm->getErrorSchema() == "" && $sf_user->hasFlash('success_title')): ?>
        <div class="success canhide">
            <?php echo $sf_user->getFlash('success_title');?>
        </div>
    <?php endif; ?>
    
    <div class="box5SubTi">
        <?php echo __('lbl_add_advertisement');?>
    </div>
    
    <div class="editStoryTable">
        <?php
            echo jq_form_remote_tag(
                array(
                    'update'    => 'editAdvertisement',
                    'url'       => url_for("@add_edit_title"),
                    'script'    => true,
                    'loading'   => jq_visual_effect('fadeIn','#indicator1'),
                    'complete'  => jq_visual_effect('fadeOut','#indicator1'),
                ),
                array(
                    'name'  => $advertisementForm->getName(), 
                    'id'    => $advertisementForm->getName()
                )
            );
            echo $advertisementForm->renderHiddenFields();
            echo input_hidden_tag('id_advertisement',$idAdvertisement);
        ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="8" class="MT5">
                <tr>
                    <td align="left" valign="top" width="40%">
                        <span class="first_td">
                            <?php echo $advertisementForm['title']->renderLabel();?><span class="redText">*</span>
                        </span>
                    </td>
                    <td width="60%" align="left" valign="top">
                        <?php 
                            echo $advertisementForm['title']->render(array('class' =>'articleFormboxTextfilde'));
                            if($advertisementForm['title']->hasError()):
                                echo $advertisementForm['title']->renderError();
                            endif;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top">&nbsp;</td>
                    <td align="left" valign="top">
                        <div class="sourceAdd">
                        <?php
                          $options    = array(
                                'method'    => 'POST',
                                'update'    => 'editAdvertisement',
                                'url'       => url_for('@add_edit_title'),
                                'loading'   => jq_visual_effect('fadeIn','#indicator1'),
                                'complete'  => jq_visual_effect('fadeOut','#indicator1').'; ',
                            );
                        
                        $html_options   = array(
                            'title' => __('btn_save'), 
                            'id'    => 'saveEditHeadline',
                            'class' => 'submitButInput'
                        );

                        echo jq_submit_to_remote('saveEditHeadline', __('btn_save'), $options, $html_options); 
                        ?>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div> 
</div>    