<div>
<div class="clear">&nbsp;</div>
<?php
    $errorHandler = $sf_data->getRaw('errorHandler');
    $rollLists    = sfConfig::get('app_adroll');
    $channelAdvertisementList = array();
    $selected_roll = array();
    if($errorHandler->getErrorCount() == 0 && count($advertisementForm->getErrorSchema()) == 0):
      foreach($channelAdvertisement as $channelAdvertisement):
        $channelAdvertisementList[]   = $channelAdvertisement['channel_id'];
      endforeach;
    else:
      $selected_roll = $sf_request->getParameter('adRoll');
      $channelAdvertisementList = $sf_request->getParameter('id_channels');
    endif;
  
    if($errorHandler->getErrorCount() == 0 && $sf_user->hasFlash('success_channels')): ?>
    <div id="errorClass">
        <div class="success canhide"><?php echo $sf_user->getFlash('success_channels'); ?></div>
    </div>
<?php endif;
    if($errorHandler != '' && $errorHandler->hasError('place_it')):
        echo '<span class="redText">';
        echo $errorHandler->getError('place_it_rolls');
        echo '</span>';
    endif;
?>
	<div class="boxW2">
        <div class="boxW2Midd">
            <div class="boxW2MidTopNorma2"><span class="first_td headlineLabel"><label><?php echo __('lbl_ad_placement');?></label></span></div>
            <div>
                <div class="boxW2MidTopNorma2">
                    <?php
                        echo checkbox_tag('adRoll[]', 1,(isset($selected_roll)?((in_array(1, $selected_roll)) ? true : false):false), array('class'=>'checkBoxWidth')).'&nbsp;<span class="font13">'.__('lbl_pre_roll').'</span>';
                    ?>
                </div>
                <div class="boxW2MidTopNorma2">
                    <?php
                        echo checkbox_tag('adRoll[]', 2,(isset($selected_roll)?((in_array(2, $selected_roll)) ? true : false):false),array('class'=>'checkBoxWidth')).'&nbsp;<span class="font13">'.__('lbl_mid_roll').'</span>';
                    ?>
                </div>
                <div class="boxW2MidTopNorma2">
                    <?php
                        echo checkbox_tag('adRoll[]', 3,(isset($selected_roll)?((in_array(3, $selected_roll)) ? true : false):false),array('class'=>'checkBoxWidth')).'&nbsp;<span class="font13">'.__('lbl_post_roll').'</span>';
                    ?>
                </div>
            </div>
        </div>
        <div class="clear placeITBorderBottom">&nbsp;</div>
        <div class="clear">&nbsp;</div>
        <?php    
            if($errorHandler != '' && $errorHandler->hasError('place_it')):
                echo '<span class="redText">';
                echo $errorHandler->getError('place_it');
                echo '</span>';
            endif;
        ?>
        <div class="boxW2MidTopNorma2">
            <span class="first_td headlineLabel"><label><?php echo __('lbl_place_your_ad_to_channels');?></label></span>
        </div>
        <div class="multiSelectBox">
            <?php foreach($channels as $key => $channel): ?>
                <div class="boxW2MidTopNorma2">
                    <?php echo checkbox_tag('id_channels[]', $channel['id'],
                    (isset($channelAdvertisementList)?((in_array($channel['id'], $channelAdvertisementList)) ? true : false):false),array('class'=>'checkBoxWidth')); ?>
                    &nbsp;<span class="font13"><?php echo $channel['channel_name']; ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>