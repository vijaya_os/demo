<?php 
    $parameters = "id_advertisement=".$idAdvertisement;
    $isDisplay = ($advertisementObj)? "display:block" : "display:block";
?>

<div class="blackBoxMain" id="addEditImage">
    <div class="black5Box">
        <div class="black5BoxBg">
            <div class="box5Top" style="<?php echo $isDisplay; ?>" id="statusDiv">
                <div class="box5SubTi">
                    <?php 
                        echo __('lbl_status').': ';
                        echo '<span id="advertisementStatus">'.
                             ucfirst(($advertisementObj)?$advertisementObj->getStatus():'').'</span>';
                    ?> 
                </div>
            </div>

            <div class="blackBoxPop2" id="editAdvertisementMenu" >
                <div id="editAdvertisementMenuDiv" onclick="showHideDetailPartion('editAdvertisement', false);
                showPageContent('editAdvertisement','<?php echo url_for('@add_edit_title');?>','<?php echo $parameters;?>');">
                    <?php echo ($idAdvertisement)?__('lbl_edit_your_title'):__('lbl_add_your_title'); ?>
                </div>
            </div>

            <div class="blackBoxPop2" id="editStoryMenu" >
                <div style="<?php echo $isDisplay; ?>" id="editStoryMenuDiv" onclick="showHideDetailPartion('editStory', false);
                showPageContent('editStory','<?php echo url_for('@add_edit_advertisement');?>','<?php echo $parameters;?>');">
                    <?php echo __('lbl_using_network'); ?>
                </div>
            </div>
            <?php $parameter = $parameters.'&media_type=image';?>
            <div class="blackBoxPop2" id="uploadImageMenu" >
                <div style="<?php echo $isDisplay; ?>" id="uploadImageMenuDiv" onclick="showHideDetailPartion('uploadImage', false);
                showPageContent('uploadImage','<?php echo url_for('@upload_advertisement_media');?>','<?php echo $parameter;?>');">
                    <?php echo __('lbl_upload_your_image'); ?>
                </div>
            </div>

            <div class="blackBoxPop2" id="advertisementPlaceItMenu" >
                <div style="<?php echo $isDisplay; ?>" id="advertisementPlaceItMenuDiv" onclick="showHideDetailPartion('articlePlaceIt', false);
                showPageContent('articlePlaceIt',
                                '<?php echo url_for('@add_edit_advertisement');?>','<?php echo $parameters;?>');">
                    <?php echo __('lbl_place_it'); ?>
                </div>
            </div>
            
            <div class="box5Bottom" style="<?php echo $isDisplay; ?>" id="buttonDiv">
                <div class="publisBut" id="Published">
                    <?php 
                        echo jq_link_to_function(
                            __('btn_publish'),
                            "changeArticleStatus('".url_for('@update_article_status')."',
                                                 '$idAdvertisement','Published','articleStatus')",
                            array('title' => __('tip_publish')));
                    ?>
                </div>
                <div class="publisBut" id="Draft" style="display: none;">
                    <?php 
                        echo jq_link_to_function(
                            __('btn_draft'),
                            "changeArticleStatus('".url_for('@update_article_status')."',
                                                 '$idAdvertisement','Draft','articleStatus')",
                            array('title' => __('tip_draft')));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
