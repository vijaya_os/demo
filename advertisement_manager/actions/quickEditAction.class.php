<?php
  /**
 * quickEditAction action.
 *
 * @package    test.com
 * @subpackage Advertisement_Manager
 * @author
 * @version    
 */
class quickEditAction extends sfAdminExtendedActions
{
    /**
     * preExecutes  action
     *
     * @author
     * @access public
     * @return void     
     */
    public function preExecute()
    {
        // Define all success / error messages.
        $this->successMsg   = array(
                                1 => __('msg_record_added'),
                                2 => __('msg_record_edited'),
                                3 => __('msg_record_deleted'),
                                4 => __('msg_status_changed')
                            );
                                
        $this->errorMsg     = array(1 => __('msg_select_atleast_one'));
        $this->formName     = 'frm_list_story';
        parent::preExecute();
    }
    
    /**
     * Executes quickEditAction quick edit of advertisement
     * 
     * @param sfRequest $request A request object
     *
     * @author
     * @return void/object
     *
     */
    public function execute($request)
    {   
        $this->idAdvertisement  = $request->getParameter('id_advertisement', '');
        $this->idMedia          = $request->getParameter('id_media', '');
        
        if($this->idMedia)
            $this->mediaObj = Doctrine::getTable('Media')->find($this->idMedia);

        $this->advertisementObj   = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);
        $this->forward404Unless($this->advertisementObj);

        //  channel articles
        $this->channelAdvertisements    = Doctrine::getTable('ChannelAdvertisement')
                                            ->findByAdvertisementId($this->idAdvertisement);
        $this->channels                 = Doctrine::getTable('Channel')
                                            ->getChannelName()->fetchArray();
        //  end of channel articles

        if($request->isMethod('post'))
        {
            if($this->validator($request))
            {
                if(isset($this->imageDetails))
                {
                    $oldImage       = isset($this->mediaObj) ? $this->mediaObj->getPath() : '';
                    $this->idMedia  = $this->uploadImage(
                                        $this->idAdvertisement, 
                                        $this->idMedia,
                                        $oldImage, 
                                        $this->image, 
                                        $this->imageDetails 
                                    );
                }
                
                $advertisement  = array(
                    'title'         => $request->getParameter('title'),
                    'start_date'    => $request->getParameter('start_date'),
                    'end_date'      => $request->getParameter('end_date'),
                    'status'        => $request->getParameter('status')
                );
                Advertisement::quickUpdateAdvertisement($this->idAdvertisement, $advertisement);
                
                $this->addChannelAdvertisements($request, $this->idAdvertisement);
                $this->advertisementObj   = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);
                
                $this->getUser()->setFlash('success_quick_edit', __('msg_record_edited'));
            }
            $this->channelAdvertisements  = $request->getParameter('id_channels');
        }
        $this->setLayout('layout_popup');
    }

    /**
     * Validates validator
     * 
     * @param sfRequest $request A request object
     *
     * @author
     * @return boolean
     *
     */
    private function validator($request)
    {
        $this->startDate    = $request->getParameter('start_date');
        $this->endDate      = $request->getParameter('end_date');
        $this->image        = $request->getFiles();
        
        $this->errorHandler = new errorHandler;
        
        if(trim($request->getParameter('title')) == "")
            $this->errorHandler->setError('title', __('err_title_required'));
        
        if(strtotime($this->startDate) > strtotime($this->endDate))
            $this->errorHandler->setError('end_date', __('err_end_date_greater_then_start_date'));
        
        if($this->image['image']['tmp_name'] != '')
            $this->validatorImage($request, $this->errorHandler, $this->image);
        
        $this->validatorPlaceIt($request, $this->errorHandler);

        return ($this->errorHandler->getErrorCount() > 0) ? false : true;
    }

    /**
     * Validates validatorImage
     * 
     * @param sfRequest $request        A request object
     * @param object    $errorHandler   error hanhler object
     * @param object    $image$request  image object
     *
     * @author
     * @return void
     *
     */
    private function validatorImage($request, $errorHandler, $image)
    {
        $this->imageDetails   = getimagesize($image['image']['tmp_name']);
            
        if(!in_array($this->imageDetails['mime'], sfConfig::get('app_mime_type_image')))
            $errorHandler->setError('image', __('err_invalid_image'));
        elseif($this->image['image']['size'] > (sfConfig::get('app_max_image_upload_limit') * 1024 * 1024))
            $errorHandler->setError('image', __('msg_invalid_image_size'));
        elseif($this->imageDetails[1] < sfConfig::get('app_thumb_big_image_height'))
            $errorHandler->setError('image', __('msg_invalid_image_width_height'));
        elseif($this->imageDetails[0] < sfConfig::get('app_thumb_big_image_width'))
            $errorHandler->setError('image', __('msg_invalid_image_width_height'));
    }

    /**
     * Validates validatorPlaceIt
     * 
     * @param sfRequest $request        A request object
     * @param object    $errorHandler   error hanhler object
     *
     * @author
     * @return void
     *
     */
    private function validatorPlaceIt($request, $errorHandler)
    {
        if(count($request->getParameter('id_channels')) > 2)
        {
            $errorHandler->setError('place_it', sprintf(
                __('msg_story_max_channels_allowed'), 
                sfConfig::get('app_max_channel_for_story'))
            );
        }
    }

    /**
     * uploadImage
     * 
     * @param string    $imageName  image name
     * @param string    $uploadDir  dir path
     * @param array     $mimeType   image mime type
     * @param array     $imageDetails   uploaded image details
     * @param string    $oldImageName   old-image name
     *
     * @author
     * @return integer
     *
     */
    private function uploadImage($idAdvertisement, $idMedia, $oldImageName, $image, $imageDetails)
    {
        $pathInfo   = pathinfo($image['image']['name']);
        $imageName  = time().'.'.$pathInfo['extension'];
        
        $filePath   = sprintf(sfConfig::get('app_upload_path_advertisement_image'), $idAdvertisement);
        $uploadDir  = sfConfig::get('sf_upload_dir').$filePath;

        sfGeneral::createDir($uploadDir.'thumb');
        sfGeneral::createDir($uploadDir.'large');

        $uploadDir  = $uploadDir.$imageName;

        move_uploaded_file($image['image']['tmp_name'], $uploadDir);
        
        $this->cropImage($imageName, sfConfig::get('sf_upload_dir').$filePath, $imageDetails['mime']);
        
        $metaTags   = @id3_get_tag(sfConfig::get('sf_upload_dir').$filePath.$imageName);
        if(isset($metaTags['genre']) && $metaTags['genre'] != '')
            $metaTags['genre'] = id3_get_genre_name($metaTags['genre']);
        
        $media  = array(
            'main_type'     => sfConfig::get('app_media_main_type_image'),
            'path'          => $imageName,
            'album'         => isset($metaTags['album']) ? $metaTags['album'] : '',
            'title'         => isset($metaTags['title']) ? $metaTags['title'] : '',
            'artist'        => isset($metaTags['artist']) ? $metaTags['artist'] : ''
        );
        
        if($oldImageName != '' && file_exists(sfConfig::get('sf_upload_dir').$filePath.$oldImageName))
        {
            unlink(sfConfig::get('sf_upload_dir').$filePath.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'thumb/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'large/'.$oldImageName);
        }

        $mediaObj   = ($idMedia) ? 
                        Media::editMedia($idMedia, $media) :
                        Media::addAdvertisementMedia($idAdvertisement, $media); 
                        
        return $mediaObj->getId();
    }

    /**
     * cropImage
     * 
     * @param string    $imageName  image name
     * @param string    $uploadDir  dir path
     * @param string    $mimeType   image mime type
     *
     * @author
     * @return void
     *
     */
    private function cropImage($imageName, $uploadDir, $mimeType)
    {
        $imageName          = array($imageName, $imageName);
        $uploadDir          = array($uploadDir, $uploadDir.'thumb/', $uploadDir.'large/');
        $imageWidthHeight   = array(
                                'large'   => array(
                                            sfConfig::get('app_thumb_big_image_width'), 
                                            sfConfig::get('app_thumb_big_image_height')
                                        ), 
                                'thumb' => array(
                                            sfConfig::get('app_thumb_small_image_width'), 
                                            sfConfig::get('app_thumb_small_image_height')
                                        )
                                );

        sfGeneral::generateCrop($imageName, $uploadDir, $imageWidthHeight, true, $mimeType);
    }

    /**
     * AddChannelAdvertisement add channel-aderviertisement
     * 
     * @param sfRequest $request A request object
     * @param integer $idAdervertisement aderviertisement-id
     *
     * @author
     * @return void
     *
     */
    private function addChannelAdvertisements($request, $idAdervertisement)
    {
        $idChannels = $request->getParameter('id_channels') ? $request->getParameter('id_channels') : array();
        ChannelAdvertisement::addChannelAdvertisement($idChannels, $idAdervertisement);
    }
}
