<?php
/**
 * addEditAdvertisement action.
 *
 * @package    test.com
 * @subpackage advertisement_Manager
 * @author
 * @version    
 */

sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
class addEditAdvertisementAction extends sfActions
{
    /**
     * Executes addEditAdvertisement
     * 
     * @param sfRequest $request A request object
     *
     * @author
     * @return void/object
     *
     */
    public function execute($request)
    {
        if($this->getUser()->hasAttribute('user_id', 'sfGuardSecurityUser'))
            $idUser = $this->getUser()->getAttribute('user_id', '', 'sfGuardSecurityUser');
        else {
            $this->getUser()->setAttribute('user_id', 1, 'sfGuardSecurityUser');
            $idUser = $this->getUser()->getAttribute('user_id', '', 'sfGuardSecurityUser');;
        }
        $this->idAdvertisement       = $request->getParameter('id_advertisement','');
        $this->idChannel             = $request->getParameter('id_channel','');
        $this->idMedia               = $request->getParameter('id_media','');
        $this->mediaType             = $request->getParameter('media_type', '');
        $this->fileName              = $request->getParameter('fileName', '');
        $this->errorHandler          = new errorHandler;
                                     
        $this->advertiseObj          = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);
        //  channel articles
        $this->channelAdvertisement  = Doctrine::getTable('ChannelAdvertisement')->findByAdvertisementId($this->idAdvertisement);
        $this->channels              = Doctrine::getTable('Channel')->getChannelName()->fetchArray();
        
        if(!$this->idMedia && $this->idAdvertisement != '')
        {
            $this->medias       = Doctrine::getTable('Media')
                                ->getAdvertiseMediaList((int)$this->idChannel, $this->mediaType, (int)$this->idAdvertisement)
                                ->fetchArray();
        }
        
        if($this->idAdvertisement)
        {
            $this->mediaObj             = $this->idMedia ? Doctrine::getTable('Media')->find($this->idMedia) : '';
            $this->mediaForm            = new mediaForm($this->mediaObj);
    
            $this->advertiseObj           = $this->idAdvertisement ? Doctrine::getTable('Advertisement')->find($this->idAdvertisement) : '';
            $this->advertisementForm          = new AdvertisementForm($this->advertiseObj);
        }
        else {
            $this->mediaObj             = '';
            $this->advertiseObj           = '';
            $this->advertisementForm          = new AdvertisementForm();
        }
        
        
        if($request->isMethod('post'))
        {
            $this->ssValidationFlag = false;
            $this->reQ = 'post';   
            $formValue = $request->getParameter($this->advertisementForm->getName());
            $ssMode = $request->getParameter('mode','');
            $this->ssMode = $ssMode;  
            if($ssMode && $ssMode == 'Draft'){
                $formValue['status'] = 'Draft';
            }else{
                $formValue['status'] = 'Published';
            }
            
             if($formValue['id'] != ""){
                if(!$this->idAdvertisement){
                    $this->idAdvertisement = $formValue['id'];
                    unset($formValue['id']);
                }
                $this->medias       = Doctrine::getTable('Media')
                                    ->getAdvertiseMediaList((int)$this->idChannel, $this->mediaType, (int)$this->idAdvertisement)
                                    ->fetchArray();
                if(count($this->medias) == 0 && trim($formValue['headline']) == ""){
                    $this->ssValidationFlag = true;
                }
                $this->advertiseObj           = $this->idAdvertisement ? Doctrine::getTable('Advertisement')->find($this->idAdvertisement) : '';
                $this->advertisementForm          = new AdvertisementForm($this->advertiseObj);
            }else{
                if(count($this->medias) == 0 && trim($formValue['headline']) == ""){
                    $this->ssValidationFlag = true;
                }
            }
            
            $this->channelAdvertise  = (count($request->getParameter('id_channels')) == 0) ?
                                        array() : $request->getParameter('id_channels');

            if(!$ssMode || (isset($ssMode) && $ssMode != 'Draft')){
                if(count($this->channelAdvertise) == 0){
                    $this->errorHandler->setError('place_it', __('err_select_channel_placeIt'));
                    $this->ssValidationFlag = true;
                }else if(!$this->validetAdvForChannel($request)){
                    $this->errorHandler->setError('place_it', sprintf(
                        __('msg_channel_max_stories_allowed'),
                        sfConfig::get('app_max_channel_story_allow'))
                    );
                    $this->ssValidationFlag = true;
                }elseif(count($request->getParameter('id_channels')) > 2){
                    $this->errorHandler->setError('place_it', sprintf(
                        __('msg_story_max_channels_allowed'),
                        sfConfig::get('app_max_channel_for_story'))
                    );
                    $this->ssValidationFlag = true;
                }
            }
            
            $start_date = date('Y-m-d h:i:s',strtotime($request->getParameter('start_date')));
            $end_date = date('Y-m-d h:i:s',strtotime($request->getParameter('end_date')));
            
            if(strtotime($start_date) > strtotime($end_date)){
                $this->errorHandler->setError('publish_start_date', __('err_select_start_date_greter_end_date'));
                $this->ssValidationFlag = true;
            }
            
            $adRoll = $request->getParameter('adRoll');
            if(!is_array($adRoll) && (!$ssMode || (isset($ssMode) && $ssMode != 'Draft') )){
                $this->errorHandler->setError('place_it_rolls', __('lbl_place_your_ad_to_rolls'));
                $this->ssValidationFlag = true;
            }else if(is_array($adRoll)){
                $formValue['placement'] = implode(',', $adRoll);
            }
            
            if($this->fileName == ""){
                $this->ssValidationFlag = true;
                $this->errorHandler->setError('advertise_media', __('msg_upload_media_file'));
            }

 
            $formValue['sf_guard_user_id'] = $idUser; 
            $formValue['start_date'] = ($start_date != "")?$start_date:date('Y-m-d h:i:s'); 
            $formValue['end_date'] = ($end_date != "")?$end_date : date('Y-m-d h:i:s'); 
            $formValue['publish_date'] = date('Y-m-d h:i:s'); 

            $this->advertisementForm->bind($formValue);
            if($this->advertisementForm->isValid() && !$this->ssValidationFlag)
            {
                $this->advertisementForm->save();
                $this->getUser()->setFlash('success_advertisement', $this->idAdvertisement ? 
                                __('msg_record_edited') : __('msg_record_added'));

                if(!$this->idAdvertisement)
                {
                    $this->idAdvertisement    = $this->advertisementForm->getObject()->getId();
                    $request->setParameter('id_advertisement', $this->idAdvertisement);
                    $this->advertiseObj   = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);
                    $this->advertisementForm  = new AdvertisementForm($this->advertiseObj);
                    $this->addChannelAdvertisement($request, $this->idAdvertisement);

                    $pathInfo           = pathinfo($this->fileName);
                    $extention          = $pathInfo['extension'];
                    $fileNameWithoutExt = time();
    
                    $imageExtention     = sfConfig::get('app_allowed_image_extension');
                    $imageExtentions    = explode(',', $imageExtention);
                    $videoExtention     = sfConfig::get('app_allowed_video_extension');
                    $videoExtentions    = explode(',', $videoExtention);
    
                    if(in_array(strtolower($extention), $videoExtentions)){
                        $mediaType   = strtolower(sfConfig::get('app_media_main_type_video'));
                    } elseif(in_array(strtolower($extention), $imageExtentions)){
                        $mediaType   = strtolower(sfConfig::get('app_media_main_type_image'));
                    } else {
                        $mediaType   = strtolower(sfConfig::get('app_media_main_type_image'));
                    }
                    
                    $filePath   = sprintf(sfConfig::get('app_upload_path_advertisement_'.$mediaType), $this->idAdvertisement);
                    $uploadDir = sfConfig::get('sf_upload_dir').$filePath;
                    
                    $filePathTMP   = sfConfig::get('app_upload_path_temp');
                    $uploadDirTMP  = sfConfig::get('sf_upload_dir').$filePathTMP;
                    
                    if(file_exists($uploadDirTMP.$this->fileName)){
                        if($mediaType == 'image'){
                            //create dir for advertisement
                            $this->createDir($mediaType, $uploadDir);
                            //copy original file
                            copy($uploadDirTMP.$this->fileName, $uploadDir.$this->fileName);
                            //upload file and generate crop
                            $this->upload($uploadDir, $this->fileName, $mediaType, $this->idAdvertisement, '', $this->idMedia);
                            //unlink file from temp 
                            $this->unlinkTempImages(sfConfig::get('app_upload_path_temp'), $this->fileName);
                        }
                        if($mediaType == 'video'){
                            //create dir for advertisement and generate images from video
                            $this->generateImagesFromVideo($this->idAdvertisement, $this->fileName);
                            //upload file and generate crop
                            $this->upload($uploadDir, $this->fileName, $mediaType, $this->idAdvertisement, '', $this->idMedia);
                            //unlink file from temp 
                            $this->unlinkTempImages(sfConfig::get('app_upload_path_temp'), $this->fileName);
                        }
                    }
                }
            }
        }else{
            if($this->idAdvertisement){
                if($this->advertiseObj){
                     $this->ssMode = $this->advertiseObj->getStatus();
                     $this->reQ = '';
                }else{
                    $this->reQ = '';
                }
            }else{
                $this->reQ = 'initial';
            }
        }
        
        $this->setLayout(false);
    }

    /**
     * addChannelAdvertisement add channel-addvertisement
     *
     * @param sfRequest $request A request object
     * @param integer $idAdvertisement advertisement-id
     *
     * @author
     * @return void
     *
     */
    private function addChannelAdvertisement($request, $idAdvertisement)
    {
        $idChannels = $request->getParameter('id_channels') ? $request->getParameter('id_channels') : array();

        ChannelAdvertisement::addChannelAdvertisement($idChannels, $idAdvertisement);
    }

    /**
     * validetAdvForChannel check advertisement for channel allowd or not
     *
     * @param sfRequest $request A request object
     *
     * @author
     * @return boolean
     *
     */
    private function validetAdvForChannel($request)
    {
        $channelLists         = array_values(sfConfig::get('app_channel_list'));

        $errorFlag = true;
        foreach($request->getParameter('id_channels') as $idChannel){
            $totalChannelAdvertisement = Doctrine::getTable('ChannelAdvertisement')->getTotalChannelAdvertisement($idChannel,$request->getParameter('id_advertisement',''))->execute()->count();

            if($totalChannelAdvertisement >= sfConfig::get('app_max_channel_story_allow')){
                $errorFlag = false;
            }
        }
        return $errorFlag;
    }

    /**
     * createDir create dir for medias
     *
     * @param string    $mediaType  media type
     *
     * @author
     * @return void
     *
     */
    
    private function createDir($mediaType, $uploadDir)
    {
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_image')))
        {
            sfGeneral::createDir($uploadDir.'small');
            sfGeneral::createDir($uploadDir.'large');
            sfGeneral::createDir($uploadDir.'landing');
            sfGeneral::createDir($uploadDir.'readermode');
        }
        else
            sfGeneral::createDir($uploadDir);
    }

    /**
     * upload   uload media file
     *
     * @param string    $uploadDir  upload dir path
     * @param string    $fileName   file name
     * @param string    $mediaType  media type
     * @param integer   $idAdvertisement  Advertisement id
     * @param integer   $idChannel  channel id
     * @param integer   $idMedia    media id
     *
     * @author
     * @return void
     *
     */
    private function upload($uploadDir, $fileName, $mediaType, $idAdvertisement, $idChannel, $idMedia)
    {
        
        $filePath     = $uploadDir.$fileName;
        $metaTags     = id3_get_tag($filePath);
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_image')))
        {
            $imageContent = getimagesize(sfConfig::get('sf_web_dir').
            sprintf(sfConfig::get('app_view_path_advertisement_'.$mediaType), $idAdvertisement, $fileName));
            $filePath   = sprintf(sfConfig::get('app_upload_path_advertisement_image'), $idAdvertisement);
            
            $cropImageDetails   = array(
                                    $uploadDir.'small/' => array(
                                        sfConfig::get('app_thumb_very_small_image_width'),
                                        sfConfig::get('app_thumb_very_small_image_height')
                                    ),
                                    $uploadDir.'large/' => array(
                                        sfConfig::get('app_thumb_big_image_width'),
                                        sfConfig::get('app_thumb_big_image_height')
                                    ),
                                    $uploadDir.'landing/'   => array(
                                        sfConfig::get('app_thumb_landing_image_width'),
                                        sfConfig::get('app_thumb_landing_image_height')
                                    ),
                                    $uploadDir.'readermode/' => array(
                                        sfConfig::get('app_thumb_readermode_image_width'),
                                        sfConfig::get('app_thumb_readermode_image_height')
                                    )
                                );
                                
            sfGeneral::generateCropImage($fileName, $uploadDir, $cropImageDetails, true);
        }

        // convert genre ID to name
        if(isset($metaTags['genre']) && $metaTags['genre'] != '')
            $metaTags['genre'] = id3_get_genre_name($metaTags['genre']);

        $media  = $this->prepareMedia($fileName, $mediaType, $metaTags);

        if($idMedia) {
        
            $mediaInfo      = Doctrine::getTable('Media')->find($idMedia);
            $oldImageName   = isset($mediaInfo) ? $mediaInfo->getPath() : '';
            
            Media::editMedia($idMedia, $media);
            $this->unlinkImages($filePath, $oldImageName);
            
        } else {
        
            if($mediaType != strtolower(sfConfig::get('app_media_main_type_audio')))
                Media::addAdvertisementMedia($idAdvertisement, $media);
            else
                Media::addAudio($idChannel, $media);
        }

        //$this->getUser()->setFlash('success_media', __('msg_file_uploaded'));
    }

    /**
     * unlinkImages delete iamges from directories
     *
     * @param string    $filePath       file path
     * @param string    $oldImageName   image name
     *
     * @author
     * @return void
     *
     */
    private function unlinkImages($filePath = '', $oldImageName = '')
    {
        if($oldImageName != '' && file_exists(sfConfig::get('sf_upload_dir').$filePath.$oldImageName))
        {
            unlink(sfConfig::get('sf_upload_dir').$filePath.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'small/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'large/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'landing/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'readermode/'.$oldImageName);
        }
    }

    /**
     * unlinkTempImages delete iamges from temp
     *
     * @param string    $filePath       file path
     * @param string    $oldImageName   image name
     *
     * @author
     * @return void
     *
     */
    private function unlinkTempImages($filePath = '', $oldImageName = '')
    {
        if($oldImageName != '' && file_exists(sfConfig::get('sf_upload_dir').$filePath.$oldImageName))
        {
           unlink(sfConfig::get('sf_upload_dir').$filePath.$oldImageName);
        }
    }

    /**
     * prepareMedia prepare media array
     *
     * @param string    $imageName      image name
     * @param string    $mediaType      media type
     * @param array     $metaTags       meta tags
     *
     * @author
     * @return void
     *
     */
    private function prepareMedia($fileName = '', $mediaType = '', $metaTags = '')
    {
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_video')))
        {
            return array(
                'main_type'     => ucfirst($mediaType),
                'video_path'    => $fileName,
                'path'          => sfContext::getInstance()->getRequest()->getParameter('path'),
                'thumb_path'    => sfContext::getInstance()->getRequest()->getParameter('path'),
                'album'         => sfContext::getInstance()->getRequest()->getParameter('album'),
                'title'         => sfContext::getInstance()->getRequest()->getParameter('title'),
                'artist'        => sfContext::getInstance()->getRequest()->getParameter('artist'),
                'video_url'     => sfContext::getInstance()->getRequest()->getParameter('video_url')
            );
        }
        else
        {
            return array(
                'main_type'     => ucfirst($mediaType),
                'path'          => $fileName,
                'album'         => $metaTags['album'],
                'title'         => $metaTags['title'],
                'artist'        => $metaTags['artist'],
                'video_url'     => '',
                'video_path'    => ''
            );
        }
    }

    /**
     * Executes generateImagesFromVideo function
     *
     * @param   integer   $idAdvertisement advertisement id
     * @param   string    $fileName file name
     *
     * @author
     * @access private
     * 
     * @return void
     *
     */
    private function generateImagesFromVideo($idAdvertisement, $fileName){
        //  set path
        $videoPath          = sfConfig::get('app_upload_path_temp');
        $imageSmallPath     = sprintf(sfConfig::get('app_upload_path_advertisement_small_image'), $idAdvertisement);
        $imageLargePath     = sprintf(sfConfig::get('app_upload_path_advertisement_large_image'), $idAdvertisement);
        $imageLandingPath   = sprintf(sfConfig::get('app_upload_path_advertisement_landing_image'), $idAdvertisement);
        $readermodePath         = sprintf(sfConfig::get('app_upload_path_readermode_adv_image'), $idAdvertisement);
        
        $preFileName        = explode('.', $fileName);
        $imageName          = $preFileName[0].'.jpg';
        $uploadDir          = sfConfig::get('sf_upload_dir');

        //  create dir
        sfGeneral::createDir($uploadDir.$imageSmallPath);
        sfGeneral::createDir($uploadDir.$imageLargePath);
        sfGeneral::createDir($uploadDir.$imageLandingPath);
        sfGeneral::createDir($uploadDir.$readermodePath);

        //  set destination path
        $sourcePath             = $uploadDir.$videoPath.$fileName;
        $destinationSmallPath   = $uploadDir.$imageSmallPath.$imageName;
        $destinationLargePath   = $uploadDir.$imageLargePath.$imageName;
        $destinationLandingPath = $uploadDir.$imageLandingPath.$imageName;
        $destinationReadermodePathPath      = $uploadDir.$readermodePath.$imageName;

        //  set created image width/height
        $bigImageSize       = sfConfig::get('app_thumb_big_image_width'). '*' .sfConfig::get('app_thumb_big_image_height');
        $smallImageSize     = sfConfig::get('app_thumb_very_small_image_width'). '*' .sfConfig::get('app_thumb_very_small_image_height');
        $landingImageSize   = sfConfig::get('app_thumb_landing_image_width').'*'.sfConfig::get('app_thumb_landing_image_height');
        $readermodeImageSize            = sfConfig::get('app_thumb_readermode_image_width').'*'.sfConfig::get('app_thumb_readermode_image_height');

        $delaySecond = $this->getDelayTime($this->getVideoDuration($sourcePath));

        exec("ffmpeg -itsoffset -".$delaySecond." -i $sourcePath -vcodec mjpeg -vframes 1 -an -f rawvideo -s $bigImageSize $destinationLargePath");
        exec("ffmpeg -itsoffset -".$delaySecond." -i $sourcePath -vcodec mjpeg -vframes 1 -an -f rawvideo -s $smallImageSize $destinationSmallPath");
        exec("ffmpeg -itsoffset -".$delaySecond." -i $sourcePath -vcodec mjpeg -vframes 1 -an -f rawvideo -s $landingImageSize $destinationLandingPath");
        exec("ffmpeg -itsoffset -".$delaySecond." -i $sourcePath -vcodec mjpeg -vframes 1 -an -f rawvideo -s $readermodeImageSize $destinationReadermodePathPath");
        sfContext::getInstance()->getRequest()->setParameter('path', $imageName);
    }
    /**
     * Executes getVideoDuration function
     *
     * @param   string  $sourceVideo    video file
     *
     * @author
     * @access private
     * @return void
     *
     */
    private function getVideoDuration($sourceVideo)
    {
        if($sourceVideo == '' || !file_exists($sourceVideo)) return false;

            ob_start();
            passthru("ffmpeg -i \"". $sourceVideo . "\" 2>&1");
            $duration = ob_get_contents();
            ob_end_clean();

            preg_match('/Duration: (.*?),/', $duration, $matches);

            return (count($matches) > 0) ? $matches[1] : 0;
    }

    /**
     * Executes getDelayTime function
     *
     * @param   string  $duration    video duration
     * @param   integer  $limit    set limit to check second
     * @param   integer  $minSecond    set minimum second
     * @param   integer  $maxSecond    set maximum second
     *
     * @author
     * @access private
     * @return void
     *
     */
    private function getDelayTime($duration, $limit = 10, $minSecond = 2, $maxSecond = 8)
    {
        if($duration && $duration == '') return false;

        $seconds = 0;
        if(Count($duration) > 0)
        {
            $time = explode(':', $duration);
            $seconds = ($time[0] <= 0 && $time[1] <= 0 && $time[2] <= $limit) ? $minSecond : $maxSecond;
        }
        return $seconds;
    }

}
