<?php
/**
 * uploadMedia actions.
 *
 * @package   
 * @subpackage advertisement_Manager
 * @author    
 * @version
 */
sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
class uploadMediaAction extends sfAction
{
    /**
     * Executes uploadMediaAction upload media (image/audio file)
     *
     * @param sfRequest $request A request object
     *
     * @author
     * @return void|object
     *
     */
    public function execute($request)
    {
        $channelLists   = sfConfig::get('app_channel_list');
        $myChannelId    = $channelLists['getBookmarksByUser'];
        
        if($this->getUser()->hasAttribute('user_id', 'sfGuardSecurityUser'))
            $idUser = $this->getUser()->getAttribute('user_id', '', 'sfGuardSecurityUser');
        else {
            $this->getUser()->setAttribute('user_id', 1, 'sfGuardSecurityUser');
            $idUser = $this->getUser()->getAttribute('user_id', '', 'sfGuardSecurityUser');
        }
        
        $this->idChannel    = $request->getParameter('id_channel', '');
        $this->idAdvertisement    = $request->getParameter('id_advertisement', '');
        $this->idMedia      = $request->getParameter('id_media', '');
        $this->mediaType    = $request->getParameter('media_type', '');
        $this->channelObj   = Doctrine::getTable('Channel')->find($this->idChannel);

        if(!$this->mediaType)
            $this->mediaType    = $request->getParameter('update');

        if($request->isMethod('post') && !$request->getParameter('see_more'))
        {
            $fileName       = $request->getParameter('qqfile');
            $mediaType      = $request->getParameter('update');
            if(!$fileName)
            {
                $files      = $request->getFiles('qqfile');
                $fileName   = $files['name'];
            }

            $pathInfo           = pathinfo($fileName);
            $extention          = $pathInfo['extension'];
            $fileNameWithoutExt = time();

            $imageExtention     = sfConfig::get('app_allowed_image_extension');
            $imageExtentions    = explode(',', $imageExtention);
            $videoExtention     = sfConfig::get('app_allowed_video_extension');
            $videoExtentions    = explode(',', $videoExtention);

            if(in_array($extention, $videoExtentions))
                $mainType   = strtolower(sfConfig::get('app_media_main_type_video'));
            elseif(in_array($extention, $imageExtentions))
                $mainType   = strtolower(sfConfig::get('app_media_main_type_image'));

            sfContext::getInstance()->getRequest()->setParameter('update', $mainType);
            $this->mediaType    = $request->getParameter('update');
        }

        $this->medias       = Doctrine::getTable('Media')
                                ->getAdvertiseMediaList((int)$this->idChannel, $this->mediaType, (int)$this->idAdvertisement)
                                ->fetchArray();

        $this->idMedia = isset($this->medias[0]['AdvertisementMedia'][0]['media_id']) ? $this->medias[0]['AdvertisementMedia'][0]['media_id'] : '';
        if($request->getParameter('see_more'))
        {
            $this->renderPartial('seeMoreUrl',
                                    array(
                                        'idChannel'    => $this->idChannel,
                                        'idAdvertisement'    => $this->idAdvertisement,
                                        'media_type'   => $this->mediaType,
                                        'medias'       => $this->medias
                                    )
                                );
            return sfView::NONE;
        }

        $this->advertisementObj   = $this->idAdvertisement ? Doctrine::getTable('Advertisement')->find($this->idAdvertisement) : '';
        if($request->isMethod('post'))
        {
            $this->advertisementForm  = ($this->idAdvertisement != "") ? new AdvertisementForm($this->advertisementObj) : new AdvertisementForm();
            $this->idChannel = 1;
            if(!$this->idAdvertisement)
            {
                /*$this->advertisementForm->bind(
                              array('sf_guard_user_id'  => $idUser,
                                    'start_date' => date('Y-m-d h:i:s'),
                                    'end_date' => date('Y-m-d h:i:s'),
                                    'publish_date' => date('Y-m-d h:i:s'),
                                    'status' => 'Draft'
                                ));
                if($this->advertisementForm->isValid())
                {
                    $this->advertisementForm->save();
                    $this->getUser()->setFlash('success_advertisement', $this->idAdvertisement ? 
                                __('msg_record_edited') : __('msg_record_added'));
                }
                $this->idAdvertisement    = $this->advertisementForm->getObject()->getId();
                $this->getResponse()->setCookie('idAdvertisement', $this->idAdvertisement);
                $this->getResponse()->setCookie('idChannel', $this->idChannel);
                $request->setParameter('id_advertisement', $this->idAdvertisement);
                $this->advertisementObj   = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);
                $this->advertisementForm  = new AdvertisementForm($this->advertisementObj);
                */
                $pathInfo           = pathinfo($fileName);
                $extention          = $pathInfo['extension'];
                $fileNameWithoutExt = time();

                $imageExtention     = sfConfig::get('app_allowed_image_extension');
                $imageExtentions    = explode(',', $imageExtention);
                $videoExtention     = sfConfig::get('app_allowed_video_extension');
                $videoExtentions    = explode(',', $videoExtention);

                if(in_array(strtolower($extention), $videoExtentions))
                    $mainType   = strtolower(sfConfig::get('app_media_main_type_video'));
                elseif(in_array(strtolower($extention), $imageExtentions))
                    $mainType   = strtolower(sfConfig::get('app_media_main_type_image'));

                sfContext::getInstance()->getRequest()->setParameter('update', $mainType);
                $this->mediaType    = $mainType;

                $this->idMedia = '';
                $this->uploadMedia(
                    $this->idChannel,
                    $this->idAdvertisement,
                    $this->idMedia,
                    $this->mediaType,
                    $fileNameWithoutExt,
                    $extention
                );
            }else{
                $this->uploadMedia(
                    $this->idChannel,
                    $this->idAdvertisement,
                    $this->idMedia,
                    $this->mediaType,
                    $fileNameWithoutExt,
                    $extention
                );
            }
            return sfView::NONE;
        }
    }

    /**
     * uploadMedia upload file and update metadata
     *
     * @param string    $idChannel  channel-id
     * @param string    $idAdvertisement  Advertisement-id
     * @param string    $idMedia    media-id
     * @param string    $mediaType  media-type
     * @param string    $fileNameWithoutExt   file name without extention
     * @param string    $extention  file extention
     *
     * @author
     * @return void
     *
     */
    public function uploadMedia($idChannel, $idAdvertisement, $idMedia, $mediaType, $fileNameWithoutExt, $extention)
    {
        $allowedExtensions  = array();
        $allowedExtension   = sfConfig::get('app_allowed_'.$mediaType.'_extension');
        $allowedExtensions  = explode(',', $allowedExtension);

        // max file size in bytes
        $sizeLimit          = sfConfig::get('app_max_'.$mediaType.'_upload_limit') *
                                sfConfig::get('app_bytes') * sfConfig::get('app_bytes');

        $uploaFileObj       = new qqFileUploader($allowedExtensions, $sizeLimit);

        //This part generates the file name with timestamp and replace the file name with time stamp
        //$filePath   = sprintf(sfConfig::get('app_upload_path_advertisement_'.$mediaType), $idAdvertisement);
        
        $filePath   = sfConfig::get('app_upload_path_temp');
        $uploadDir  = sfConfig::get('sf_upload_dir').$filePath;
        if($idAdvertisement != "")
            $this->createDir($mediaType, $uploadDir);
        else 
            $this->createDir('', $uploadDir);

        //  file name with extentions
        $fileName               = $fileNameWithoutExt.'.'.$extention;
        //File upload part
        $this->uploadMessage    = $uploaFileObj->handleUpload($uploadDir, false, $fileNameWithoutExt, $mediaType);

        $this->getResponse()->setCookie('uploadedFile', $fileName);
        $this->getResponse()->setCookie('error', 'Valid');

        if(preg_match('/success/i', json_encode($this->uploadMessage)))
        {
            //$this->upload($uploadDir, $fileName, $mediaType, $idAdvertisement, $idChannel, $idMedia);
        }
        else
        {
            if (preg_match('/Invalid/i', $this->uploadMessage['error']) ||
                preg_match('/size/i', $this->uploadMessage['error']))
            {
                $errorMessage = str_replace(" " , "_", $this->uploadMessage['error']);
                $this->getResponse()->setCookie('error', $errorMessage);
            }
        }
    }

    /**
     * upload   uload media file
     *
     * @param string    $uploadDir  upload dir path
     * @param string    $fileName   file name
     * @param string    $mediaType  media type
     * @param integer   $idAdvertisement  Advertisement id
     * @param integer   $idChannel  channel id
     * @param integer   $idMedia    media id
     *
     * @author
     * @return void
     *
     */
    private function upload($uploadDir, $fileName, $mediaType, $idAdvertisement, $idChannel, $idMedia)
    {
        
        $filePath     = $uploadDir.$fileName;
        $metaTags     = id3_get_tag($filePath);
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_image')))
        {
            $imageContent = getimagesize(sfConfig::get('sf_web_dir').
            sprintf(sfConfig::get('app_view_path_advertisement_'.$mediaType), $idAdvertisement, $fileName));
            $filePath   = sprintf(sfConfig::get('app_upload_path_advertisement_image'), $idAdvertisement);
            
            $cropImageDetails   = array(
                                    $uploadDir.'small/' => array(
                                        sfConfig::get('app_thumb_very_small_image_width'),
                                        sfConfig::get('app_thumb_very_small_image_height')
                                    ),
                                    $uploadDir.'large/' => array(
                                        sfConfig::get('app_thumb_big_image_width'),
                                        sfConfig::get('app_thumb_big_image_height')
                                    ),
                                    $uploadDir.'landing/'   => array(
                                        sfConfig::get('app_thumb_landing_image_width'),
                                        sfConfig::get('app_thumb_landing_image_height')
                                    ),
                                    $uploadDir.'readermode/' => array(
                                        sfConfig::get('app_thumb_readermode_image_width'),
                                        sfConfig::get('app_thumb_readermode_image_height')
                                    )
                                );
                                
            sfGeneral::generateCropImage($fileName, $uploadDir, $cropImageDetails, true);
        }

        // convert genre ID to name
        if(isset($metaTags['genre']) && $metaTags['genre'] != '')
            $metaTags['genre'] = id3_get_genre_name($metaTags['genre']);

        $media  = $this->prepareMedia($fileName, $mediaType, $metaTags);

        if($idMedia) {
        
            $mediaInfo      = Doctrine::getTable('Media')->find($idMedia);
            $oldImageName   = isset($mediaInfo) ? $mediaInfo->getPath() : '';
            
            Media::editMedia($idMedia, $media);
            $this->unlinkImages($filePath, $oldImageName);
            
        } else {
        
            if($mediaType != strtolower(sfConfig::get('app_media_main_type_audio')))
                Media::addAdvertisementMedia($idAdvertisement, $media);
            else
                Media::addAudio($idChannel, $media);
        }

        $this->getUser()->setFlash('success_media', __('msg_file_uploaded'));
    }

    /**
     * createDir create dir for medias
     *
     * @param string    $mediaType  media type
     *
     * @author
     * @return void
     *
     */
    private function createDir($mediaType, $uploadDir)
    {
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_image')))
        {
            sfGeneral::createDir($uploadDir.'small');
            sfGeneral::createDir($uploadDir.'large');
            sfGeneral::createDir($uploadDir.'landing');
            sfGeneral::createDir($uploadDir.'readermode');
        }
        else
            sfGeneral::createDir($uploadDir);
    }

    /**
     * unlinkImages delete iamges from directories
     *
     * @param string    $filePath       file path
     * @param string    $oldImageName   image name
     *
     * @author
     * @return void
     *
     */
    private function unlinkImages($filePath = '', $oldImageName = '')
    {
        if($oldImageName != '' && file_exists(sfConfig::get('sf_upload_dir').$filePath.$oldImageName))
        {
            unlink(sfConfig::get('sf_upload_dir').$filePath.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'small/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'large/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'landing/'.$oldImageName);
            unlink(sfConfig::get('sf_upload_dir').$filePath.'readermode/'.$oldImageName);
        }
    }

    /**
     * prepareMedia prepare media array
     *
     * @param string    $imageName      image name
     * @param string    $mediaType      media type
     * @param array     $metaTags       meta tags
     *
     * @author
     * @return void
     *
     */
    private function prepareMedia($fileName = '', $mediaType = '', $metaTags = '')
    {
        if(strtolower($mediaType) == strtolower(sfConfig::get('app_media_main_type_video')))
        {
            return array(
                'main_type'     => ucfirst($mediaType),
                'video_path'    => $fileName,
                'path'          => sfContext::getInstance()->getRequest()->getParameter('path'),
                'thumb_path'    => sfContext::getInstance()->getRequest()->getParameter('path'),
                'album'         => sfContext::getInstance()->getRequest()->getParameter('album'),
                'title'         => sfContext::getInstance()->getRequest()->getParameter('title'),
                'artist'        => sfContext::getInstance()->getRequest()->getParameter('artist'),
                'video_url'     => sfContext::getInstance()->getRequest()->getParameter('video_url')
            );
        }
        else
        {
            return array(
                'main_type'     => ucfirst($mediaType),
                'path'          => $fileName,
                'album'         => $metaTags['album'],
                'title'         => $metaTags['title'],
                'artist'        => $metaTags['artist'],
                'video_url'     => '',
                'video_path'    => ''
            );
        }
    }
}
