<?php

/**
 * advertisement_manager actions.
 *
 * @package    
 * @subpackage advertisement_manager
 * @author     
 * @version
 */
sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
class advertisement_managerActions extends sfAdminExtendedActions
{
    /**
     * preExecutes  action
     *
     * @author 
     * @access public
     * @return void
     */
    public function preExecute()
    {
        // Define all success / error messages.
        $this->successMsg   = array(
                                1 => __('msg_record_added'),
                                2 => __('msg_record_edited'),
                                3 => __('msg_record_deleted'),
                                4 => __('msg_status_changed')
                            );

        $this->errorMsg     = array(1 => __('msg_select_atleast_one'));
        $this->formName     = 'frm_list_advertisement';
        parent::preExecute();
    }

    /**
     * Executes index member listing action
     *
     * @param sfRequest $request A request object
     *
     * @author 
     * @return void
     *
     */
    public function executeIndex(sfWebRequest $request)
    {
        // Set combo properties
        $this->search        = array(
            'title'      => __('title'),
        );

	 

        if($request->getParameter('id') && $request->getParameter('admin_act'))
        {
            $this->adminAction(
               $request->getParameter('admin_act'),
               $request->getParameter('id'),
               $request->getParameter('request_status')
            );
        }

        // Get make list for listing.
        //print_r($this->extraParameters); 
        $articleListQuery   = Doctrine::getTable('Advertisement')->getAdvertisementList($this->extraParameters);
		//echo $articleListQuery->getSqlQuery(); 
        // Set pager and get results
        $pagerObj            = new sfMyPager();
        $this->listObj       = $pagerObj->getResults('Advertisement', $this->paging, $articleListQuery, $this->page);
        $this->listRecords   = $this->listObj->getResults(Doctrine::HYDRATE_ARRAY);

        //echo "<pre>"; print_r($this->listRecords);exit;

        // Total number of records
        $this->totalRecords     = $this->listObj->getNbResults();
        $this->pageTotalRecords = count($this->listRecords);
		$this->maxPaging        = ceil($this->totalRecords/sfConfig::get('app_default_paging_records'));

        $this->refreshAgain = ($this->totalRecords <= ($this->paging * ($this->page-1))) ? true : false;

        if($request->getParameter('request_type') == 'ajax_request')
            $this->setTemplate('listContentUpdate');

        elseif($request->isXmlHttpRequest() && $request->getParameter('set_paging'))
        {
            $this->renderPartial(
                'list_middle_content',
                array(
                    'listRecords'       => $this->listRecords,
                    'extraParameters'   => $this->extraParameters,
                    'url'               => '@manage_advertisements'
                )
            );
            return sfView::NONE;
        }
    }

    /**
     * adminActions do different actions change status, delete record
     *
     * @param String  $adminAction active
     * @param Integer $idArticle   article-id
     * @param String  $status      request
     *
     * @author 
     * @return void
     *
     */
    public function adminAction($adminAction, $idAdvertisement, $status)
    {
        switch ($adminAction)
        {
            case 'delete_one':
            case 'delete':
                if(sfGeneral::deleteRecordsComposite('Advertisement', $idAdvertisement, 'id'))
                {
                    $this->getUser()->setFlash('succ_msg', $this->successMsg[3]);
                }
                break;
        }
    }

    /**
     * Executes executeQuickEdit quick-edit advertisement/article action
     *
     * @param sfRequest $request A request object
     *
     * @author 
     * @return void
     *
     */
    public function executeQuickEdit(sfWebRequest $request)
    {
        $idArticle      = $request->getParameter('id_article');
        $this->sfGuardAdminUserForm = new sfGuardAdminUserForm($sfGuardUser);

        if($request->isMethod('post'))
        {
            $userRequest        = $request->getParameter('sf_guard_admin_user');
            $memberChannels     = (isset($userRequest['channel_id']) ? $userRequest['channel_id'] : '');
            $memberCategories   = (isset($userRequest['category_id']) ? $userRequest['category_id'] : '');

            $this->sfGuardAdminUserForm->bind($request->getParameter($this->sfGuardAdminUserForm->getName()));

            if($this->sfGuardAdminUserForm->isValid())
            {
                $this->sfGuardAdminUserForm->save();
                $this->getUser()->setFlash('succ_msg', ($idArticle ? $this->successMsg[2] : $this->successMsg[1]));
                $this->redirect('@manage_advertisements?'.html_entity_decode($this->extraParameters['querystr']));
            }
        }
        $this->memberChannels   = (is_array($memberChannels) && count($memberChannels) > 0) ?
                                   implode(',', $memberChannels) : '';
        $this->memberCategories = (is_array($memberCategories) && count($memberCategories) > 0) ?
                                   implode(',', $memberCategories) : '';

        if($request->getParameter('view'))
        {
            $this->memberView(
                $memberCategories,
                $memberChannels,
                $this->sfGuardAdminUserForm
            );
            $this->setTemplate('view');
            $this->setLayout('layout_popup');
        }
    }

    /**
     * Executes addedit MemberProfile action
     *
     * @param sfRequest $request A request object
     *
     * @author 
     * @return void
     *
     */
    public function executeAddedit(sfWebRequest $request)
    {
        $this->setObjectAndVariables($request);
        if($request->isMethod('post'))
        {
            $userRequest        = $request->getParameter('sf_guard_admin_user');
            $memberChannels     = (isset($userRequest['channel_id']) ? $userRequest['channel_id'] : '');
            $memberCategories   = (isset($userRequest['category_id']) ? $userRequest['category_id'] : '');

            $this->sfGuardAdminUserForm->bind($request->getParameter($this->sfGuardAdminUserForm->getName()));

            if($this->sfGuardAdminUserForm->isValid())
            {
                $this->sfGuardAdminUserForm->save();
                $this->getUser()->setFlash('succ_msg', ($idArticle ? $this->successMsg[2] : $this->successMsg[1]));
                $this->redirect('@manage_advertisements?'.html_entity_decode($this->extraParameters['querystr']));
            }
        }

        if($request->getParameter('view'))
        {
            $this->memberView(
                $memberCategories,
                $memberChannels,
                $this->sfGuardAdminUserForm
            );
            $this->setTemplate('view');
        }
    }

    /**
     * memberView do different actions change status, delete record
     *
     * @param String $memberCategories     membercategory
     * @param String $memberChannels       memberchannels
     * @param Array  $sfGuardAdminUserForm sfGuardAdminUserForm
     *
     * @author 
     * @return void
     *
     */
    private function memberView($memberCategories, $memberChannels, $sfGuardAdminUserForm)
    {
        $this->categorySet  = $this->getCategories($memberCategories);
        $this->channelSet   = $this->getChannels($memberChannels);

        $this->timeZone     = Doctrine::getTable('TimeZone')
                                ->find($sfGuardAdminUserForm['members']['time_zone_id']->getValue())
                                ->getTimeZone();

        $this->permissionList   = Doctrine::getTable('sfGuardPermission')
                                    ->find($sfGuardAdminUserForm['permissions_list']->getValue())
                                    ->getName();

        $this->groupList    = Doctrine::getTable('sfGuardGroup')
                                ->find($sfGuardAdminUserForm['groups_list']->getValue())
                                ->getName();

        $countryName        = sfCultureInfo::getInstance()->getCountries();
        $this->countryName  = $countryName[$sfGuardAdminUserForm['members']['country_code']->getValue()];
    }

    /**
     * getCategories do different actions change status, delete record
     *
     * @param Array  $memberCategories membercategories
     *
     * @author
     * @return array
     *
     */
    private function getCategories($memberCategories)
    {
        $categories   = Doctrine::getTable('Category')->getCategories()->fetchArray();
        foreach($categories as $category)
        {
            if(in_array($category['id'], $memberCategories))
                $categorySet[]  = $category['category_name'];
        }
        return isset($categorySet) ? $categorySet : array();
    }

    /**
     * getChannels return list of channel
     *
     * @param Array  $memberChannels memberchannels
     *
     * @author 
     * @return array
     *
     */
    private function getChannels($memberChannels)
    {
        $channels   = Doctrine::getTable('Channel')->getChannels()->fetchArray();
        foreach($channels as $channel)
        {
            if(in_array($channel['id'], $memberChannels))
                $channelSet[]  = $channel['channel_name'];
        }
        return isset($channelSet) ? $channelSet : array();
    }

    /**
     * Executes GetPermissions action
     *
     * @param sfRequest $request A request object
     *
     * @author
     * @return void
     *
     */
    public function executeGetPermissions(sfWebRequest $request)
    {
        $groupId                    = $request->getParameter('group_id');
        $this->permissionId         = $request->getParameter('permission_id');
        $this->sfGuardPermission    = Doctrine::getTable('SfGuardPermission')->getPermissionByGroupId($groupId);
    }

    /**
     * Executes AddEditTitle action
     *
     * @param sfRequest $request A request object
     *
     * @author
     * @return void
     *
     */
    public function executeAddEditTitle(sfWebRequest $request)
    {
        $this->setObjectAndVariables($request);
        if($request->isMethod('post'))
        {
            $this->advertisementForm->bind($request->getParameter($this->advertisementForm->getName()));

            if($this->advertisementForm->isValid())
            {
                $this->advertisementForm->save();
                $this->addAdvertisement = $this->idAdvertisement ? false:true;
                $this->getUser()->setFlash('success_title',
                                           ($this->idAdvertisement ? $this->successMsg[2] : $this->successMsg[1]));
                $this->idAdvertisement = $this->advertisementForm->getObject()->getId();
                $this->advertisementObj  = Doctrine::getTable('Advertisement')->find($this->idAdvertisement);

            }
        }
    }
    /**
     * Executes setObjectAndVariables method for common variable,forms and model objects.
     *
     * @param sfRequest $request A request object
     *
     * @author
     * @return void
     *
     */
    private function setObjectAndVariables($request)
    {
        $this->idAdvertisement   = $request->getParameter('id_advertisement');
        $this->advertisementObj  = $this->idAdvertisement ?
                                   Doctrine::getTable('Advertisement')->find($this->idAdvertisement) : '';
        $this->advertisementForm = $this->idAdvertisement ? new AdvertisementForm($this->advertisementObj)
                                                           :new AdvertisementForm();
        $this->addAdvertisement  = true;
    }
}
