<?php

/**
 * ChannelTable
 *
 * @package    test.com
 * @subpackage Model
 * @author     Vijay   <Vijay@test.com>
 * @version    SVN: $Id: ArticleTable.class.php 7490 2010-03-29 19:53:27Z jwage $
 *
 */
class ArticleTable extends Doctrine_Table
{
    /**
     * Executes getInstance function
     *
     * @author Vijay   <Vijay@test.com>
     * @access public
     * @return object
     *
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Article');
    }

    /**
     * Executes getArticleMediaList for getting article media lists.
     *
     * @param Integer $channelId channel id
     * @param String  $mainType  media   main type (image/audio)
     *
     * @access Public
     * @author Vijay   <Vijay@test.com>
     *
     * @return Object
     */
    public function getArticleMediaList($channelId = '', $mainType = 'Image')
    {
        if($channelId == '' || !is_numeric($channelId) || !is_string($mainType) || $mainType == '')
                     return false;

        try
        {
            $articelObj = Doctrine_Query::create()
                                ->select('A.id, AM.id, AM.media_id, M.id, M.thumb_path, M.main_type, M.album, M.title,
                                    A.title, A.headline, A.publish_start_date, A.status, A.summary')
                                ->from('Article A')
                                ->leftJoin('A.ChannelArticle CA')
                                ->leftJoin('A.ArticleMedia AM')
                                ->leftJoin('AM.Media M ')
                                ->where('A.status != ?', sfConfig::get('app_status_inactive'))
                                ->andWhere('A.publish_end_date >= ?', date('y-m-d'));
            if($channelId)
                $articelObj = $articelObj->andWhere('CA.channel_id = ?', $channelId);

            $articelObj
                ->addOrderBy('CA.sort_order ASC')
                ->addOrderBy('A.updated_at DESC')
                ->addOrderBy('M.id ASC')
                ->addOrderBy('AM.id ASC');
                /*->addOrderBy('CA.sort_order ASC')
                ->addOrderBy('A.publish_start_date DESC')
                ->addOrderBy('M.main_type DESC')
                ->addOrderBy('AM.id ASC');*/

            return $articelObj;

        } catch (Doctrine_Connection_Mysql_Exception $error) {
            die('Error: '. $error->getMessage() );
        }
    }

    /**
     * Executes getArticleList getting article lists
     *
     * @param array     $extraParameters
     *
     * @access Public
     * @author Vijay   <Vijay@test.com>
     *
     * @return Object
     */
    public function getArticleList($extraParameters = array())
    {
        if(!is_array($extraParameters))
            return false;

        try
        {
            $channelObj = Doctrine_Query::create()
                            ->select('GROUP_CONCAT(C.channel_name)')
                            ->from('Channel C')
                            ->leftJoin('C.ChannelArticle CA')
                            ->where('FIND_IN_SET(c3.article_id, a.id)');

            $articelObj = Doctrine_Query::create()
                            ->select(' CAA.id, A.id, A.title, A.headline, A.status, A.headline,
                                DATE_FORMAT(A.publish_start_date,"%m/%d/%Y<br>%h:%i:%s") as publish_start_date,
                                A.publish_start_date as start_date,
                                AM.id, AM.media_id,
                                M.path, M.main_type , ('.$channelObj->getDql().') as channel_name')
                            ->from('Article A')
                            ->leftJoin('A.ChannelArticle CAA')
                            ->leftJoin('A.ArticleMedia AM')
                            ->leftJoin('AM.Media M')
                            ->addOrderBy('A.id DESC')
                            ->addOrderBy('A.publish_start_date DESC')
                            ->addOrderBy('M.main_type DESC')
                            ->addOrderBy('M.id ASC');

            if(count($extraParameters) > 0)
                $articelObj = sfGeneral::setCriteria($articelObj, $extraParameters);

            if(isset($extraParameters['searchValue']) && trim($extraParameters['searchValue']) != '')
            {
                $channels   = Doctrine_Query::create()
                            ->select('C.id')
                            ->from('Channel C')
                            ->where('C.channel_name LIKE ?', "%".trim($extraParameters['searchValue'])."%")
                            ->fetchArray();

                if(count($channels) > 0)
                {
                    foreach($channels as $channel)
                        $channelId[]   = $channel['id'];
                }

                $articelObj->addWhere(
                    'A.headline LIKE ? OR CAA.channel_id IN ?',
                    array(
                        "%".$extraParameters['searchValue']."%",
                        $channelId
                    )
                );
            }
        } catch ( Doctrine_Connection_Mysql_Exception $error ) {
            die( 'Error: '. $error->getMessage() );
        }
            return $articelObj;
    }

    /**
     * Executes getArticleById get articles by id-article
     *
     * @param Integer $articleId article id
     *
     * @access Public
     * @author Mahendra   <vijay@test.com>
     *
     * @return object
     */
    public static function getArticleById($articleId = '')
    {
        if($articleId == '' || !is_numeric($articleId)){
            return false;
        }

        try
        {
            $articelObj = Doctrine_Query::create()
                                ->select('A.id, A.headline, A.title')
                                ->from('Article A')
                                ->where('A.id = ?', $articleId)
                                ->andWhere('A.status != ?', sfConfig::get('app_status_inactive'));
            return $articelObj;
        } catch ( Doctrine_Connection_Mysql_Exception $error ) {
            die( 'Error: '. $error->getMessage() );
        }
    }

    /**
     * Executes getArticleByChannelId get articles by id-channel
     *
     * @param Integer $channelId channel id
     *
     * @access Public
     * @author Vijay   <Vijay@test.com>
     *
     * @return array
     */
    public function getArticleByChannelId($channelId = '')
    {
        if($channelId == '' || !is_numeric($channelId))
            return false;

        try
        {
            return Doctrine_Query::create()
                    ->select('A.id, AM.id, AM.media_id, M.id, M.thumb_path, M.main_type, M.album, M.title,
                                A.sf_guard_user_id, A.title, A.headline, A.publish_start_date, A.summary')
                    ->from('Article A')
                    ->leftJoin('A.ChannelArticle CA')
                    ->leftJoin('A.ArticleMedia AM')
                    ->leftJoin('AM.Media M')
                    ->andWhere('CA.channel_id = ?', $channelId)
                    ->addOrderBy('AM.id ASC')
                    ->fetchArray();

        } catch (Doctrine_Connection_Mysql_Exception $error) {
            die('Error: '. $error->getMessage() );
        }
    }

    /**
     * Executes getLandingPageArticles get articles
     *
     * @access Public
     * @author Vijay   <Vijay@test.com>
     *
     * @return array
     */
    public function getLandingPageArticles()
    {
        try
        {
            $readArticleObj = Doctrine_Query::create()
                                ->select('count(RA.id)')
                                ->from('ReadArticle RA')
                                ->where('RA.article_id = A.id')
                                ->groupBy('RA.article_id');

            return Doctrine::getTable('Channel')
                                ->getChannelsList(array())
                                ->addSelect(
                                    'DATE_FORMAT(A.publish_start_date,"%m/%d/%Y<br>%h:%i:%s") as publish_start_date,
                                    A.publish_start_date as start_date, MC.path as audio_path, CM.id, MC.id'
                                )
                                ->leftJoin('C.ChannelMedia CM')
                                ->leftJoin('CM.Media MC')
                                ->where('C.status = ? AND (A.is_sticky = "Yes" OR A.publish_end_date >= ?)',
                                            array(sfConfig::get('app_status_published'),date('Y-m-d')));
        } catch (Doctrine_Connection_Mysql_Exception $error) {
            die('Error: '. $error->getMessage() );
        }
    }

    /**
     * Executes getSingleArticleById get full detail of article
     *
     * @param Integer $article_id article id
     *
     * @access Public
     * @author Vijay Amin  <vijay@test.com>
     *
     * @return array
     */
    public static function getSingleArticleById($article_id="")
    {
        if($article_id == "" || !is_numeric($article_id)) return false;

        try
        {
            return Doctrine_Query::create()
                    ->select('A.id, AM.id, AM.media_id, M.id, M.thumb_path, M.main_type, M.album, M.title,
                                A.sf_guard_user_id, A.title, A.headline, A.content, A.publish_start_date, A.summary')
                    ->from('Article A')
                    ->leftJoin('A.ChannelArticle CA')
                    ->leftJoin('A.ArticleMedia AM')
                    ->leftJoin('AM.Media M')
                    ->andWhere('A.id = ?', $article_id)
                    ->addGroupBy('A.id')
                    ->fetchArray();

        } catch (Doctrine_Connection_Mysql_Exception $error) {
            die('Error: '. $error->getMessage() );
        }
    }

}
